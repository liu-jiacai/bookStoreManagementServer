//引入相关模块
const mysql = require("../Controller/OperateDB/mysql.js");
const BCrypt = require("../Controller/Function/Bcrypt.js");
const DocumentFunction = require("../Controller/Function/DocumentFunction.js");
const OperationalObjectives = require("./Function/OperationalObjectives.js");
const jwt = require('jsonwebtoken');
const keys = require("../config/keys.js");
const path = require("path");
const Config = require("../config/config.js");
const TraversingArray = require("./Function/TraversingArray.js");
const NXlSX = require("node-xlsx");

//处理用户注册逻辑业务
module.exports.doRegister = async (ctx) => {
    try {
        // 获取客户端数据
        let postParams = ctx.request.body;
        //判断参数
        if (!postParams.hasOwnProperty("name") || postParams.name === "" || postParams.name === undefined) {
            ctx.body = {
                code: -1,
                message: "请填写用户账号",
                data: ""
            };
            return;
        }
        if (!postParams.hasOwnProperty("pwd") || postParams.pwd === "" || postParams.pwd === undefined) {
            ctx.body = {
                code: -2,
                message: "请填写用户密码",
                data: ""
            };
            return;
        }
        if (!postParams.hasOwnProperty("nickname") || postParams.nickname === "" || postParams.nickname === undefined) {
            ctx.body = {
                code: -3,
                message: "请填写用户昵称",
                data: ""
            };
            return;
        }
        if (!/^1[3456789]\d{9}$/.test(postParams.name)) {
            ctx.body = {
                code: -4,
                message: "服务器端发现你的账号格式不正确",
                data: ""
            };
            return;
        }
        if (!/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/.test(postParams.pwd)) {
            ctx.body = {
                code: -5,
                message: "服务器端发现你的密码格式不正确",
                data: ""
            };
            return;
        }

        //判断数据库中有没有相同的数据记录
        let hasUser = await mysql.query("select * from ??  where account=?", ["shop_admin_user", postParams.account]);
        if (hasUser.length > 0) {
            ctx.body = {code: -6, message: "该账号已经注册过了", data: ""};
            return;
        }

        //对密码进行加盐加密处理
        let password = await BCrypt.SaltEncryption(postParams.pwd);
        //默认头像
        let defaultAvatar = Config.baseURL + ":" + Config.port + "/images/default/default.jpg";

        // 构建数据数组
        let insertData = [null, postParams.name, password, postParams.nickname, defaultAvatar, new Date().toLocaleString(), new Date().toLocaleString()];
        //插入数据
        let sql = "insert into ?? values ?";
        let returnResult = await mysql.query(sql, ["shop_admin_user", [insertData]]);
        if (returnResult.serverStatus) {
            ctx.body = {code: 200, message: "注册成功", data: ""};
        } else {
            ctx.body = {code: 500, message: "注册失败", data: returnResult};
        }
    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//处理用户登录逻辑业务
module.exports.doLogin = async (ctx) => {
    try {
        //获取客户端数据
        let postParams = ctx.request.body;

        //检查数据有性
        if (!postParams.hasOwnProperty("account") || postParams.account === "" || postParams.account === undefined) {
            ctx.body = {
                code: -1,
                message: "请输入用户账号",
                data: ""
            };
            return;
        }
        if (!postParams.hasOwnProperty("password") || postParams.password === "" || postParams.password === undefined) {
            ctx.body = {
                code: -2,
                message: "请输入用户密码",
                data: ""
            };
            return;
        }
        if (!/^1[3456789]\d{9}$/.test(postParams.account)) {
            ctx.body = {
                code: -3,
                message: "服务器端发现你的账号格式不正确",
                data: ""
            };
            return;
        }
        if (!/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/.test(postParams.password)) {
            ctx.body = {
                code: -4,
                message: "服务器端发现你的密码格式不正确",
                data: ""
            };
            return;
        }

        //检查数据中是否有该数据
        let returnArray = await mysql.query("select * from ??  where account=?", ["shop_admin_user", postParams.account]);
        if (returnArray.length <= 0) {
            ctx.body = {code: -5, message: "该账号没有注册，请先去注册", data: ""};
            return;
        }
        //大于0的情况
        let isMatch = await BCrypt.PasswordCompare(postParams.password, returnArray[0].password);
        if (isMatch) {
            // 定义token的规则
            const rule = {
                userId: returnArray[0].id,
                nickName: returnArray[0].nickname,
                account: returnArray[0].account,
                avatar: returnArray[0].avatarUrl,
            };
            // 生成token一个小时过期
            let token = await jwt.sign(rule, keys.secretOrPrivateKey, {expiresIn: "1h"});
            ctx.body = {
                code: 200,
                message: '登录成功',
                token: 'Bearer ' + token
            }
        } else {
            ctx.body = {code: -6, message: "密码错误", data: ""};
        }
    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

// 修改用户的昵称逻辑业务
module.exports.changeNickName = async (ctx) => {
    try {
        // 客户端信息
        let postParams = ctx.request.body;
        // 检查数据有效性
        if (!postParams.hasOwnProperty("userId") || postParams.userId === "" || postParams.userId === undefined) {
            ctx.body = {code: -1, message: "缺少唯一标识，无法修改用户的昵称", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("nickName") || postParams.nickName === "" || postParams.nickName === undefined) {
            ctx.body = {code: -2, message: "用户昵称不能为空", data: ""};
            return;
        }

        // 操作数据库
        let result = await mysql.query("select * from ?? where id = ?", ["shop_admin_user", postParams.userId]);
        if (result.length <= 0) {
            ctx.body = {code: -3, message: "该用户不存在", data: ""};
            return;
        }

        let returnResult = await mysql.query("update ?? set nickname=? where id = ?", ["shop_admin_user", postParams.nickName, postParams.userId]);
        if (returnResult) {
            ctx.body = {code: 200, message: "修改成功", data: returnResult}
        }
    } catch (error) {
        ctx.body = {
            code: 500,
            message: "服务器端错误",
            data: error
        }
    }
};

//修改用户的头像的逻辑业务
module.exports.changeAvatar = async (ctx) => {
    try {
        // 获取客户端数据
        let userId = ctx.request.body.userId;
        let file = ctx.request.files;

        //检查文件上传的文件为空的时候
        if (file.avatar.name === "") {
            let result = await DocumentFunction.deleteFile(file.avatar.path);
            if (result === -1) {
                ctx.body = {code: -1, message: "服务端删除临时文件失败", data: "name"};
                return;
            }
            ctx.body = {code: -2, message: "请上传用户的头像", data: ""};
            return;
        }

        // 检查文件的大小
        if (file.avatar.size > 1024 * 500) {
            let result = await DocumentFunction.deleteFile(file.avatar.path);
            if (result === -1) {
                ctx.body = {code: -1, message: "服务端删除临时文件失败", data: "size"};
                return;
            }
            ctx.body = {code: -3, message: "上传的文件大小不能超过500KB", data: ""};
            return;
        }

        //检查文件的类型
        let typeCollection = [".jpg", ".png", ".JPG", ".PNG"];
        if (!typeCollection.includes(path.extname(file.avatar.path))) {
            let result = await DocumentFunction.deleteFile(file.avatar.path);
            if (result === -1) {
                ctx.body = {code: -1, message: "服务端删除临时文件失败", data: "type"};
                return;
            }
            ctx.body = {code: -4, message: "请检查上传文件的类型", data: ""};
            return;
        }

        //检查用户的Id是否上传
        if (userId === "" || userId === undefined) {
            let result = await DocumentFunction.deleteFile(file.avatar.path);
            if (result === -1) {
                ctx.body = {code: -1, message: "服务端删除临时文件失败", data: "userId"};
                return;
            }
            ctx.body = {code: -5, message: "用户ID不能为空", data: ""};
            return;
        }

        // 检查用户是否存在
        let result = await mysql.query("select * from ?? where id = ?", ["shop_admin_user", userId]);
        if (result.length <= 0) {
            let result = await DocumentFunction.deleteFile(file.avatar.path);
            if (result === -1) {
                ctx.body = {code: -1, message: "服务端删除临时文件失败", data: "userLength"};
                return;
            }
            ctx.body = {code: -6, message: "该用户不存在", data: ""};
            return;
        }

        // 保存旧头像文件的地址
        let avatar = result[0].avatarUrl;

        //创建文件的名称
        let fileName = await DocumentFunction.createFileName(new Date()) + path.extname(file.avatar.name);

        // 修改文件名称
        let renameFile = await DocumentFunction.renameFile(file.avatar.path, path.join(__dirname, "../public/images/user", fileName));
        if (renameFile === -1) {
            ctx.body = {code: -7, message: "服务端重名命文件失败", data: "renameFile"};
            return;
        }

        //修改数据库的数据
        let sql = "update ?? set avatarUrl=? where id=?";
        let SrcUrl = Config.baseURL + ":" + Config.port + "/images/user/" + fileName;
        let updateResult = await mysql.query(sql, ["shop_admin_user", SrcUrl, userId]);
        //修改成功后
        if (updateResult.serverStatus) {
            //修改成功后，查找到用户检查用户的ID使用的原来的头像进行删除（除了默认头像）判断头像的地址是不是默认 (true表示没有找到)
            if (avatar.indexOf("/images/default/default.jpg") === -1) {
               // http://127.0.0.1:7070/images/user/20 2103 0212 5616 3729 .jpg
                let oldFileName = avatar.toString().substring(avatar.length - 22);
                let result = await DocumentFunction.deleteFile(path.join(__dirname, "../public/images/user/", oldFileName));
                if (result === -1) {
                    ctx.body = {code: -7, message: "服务端删除旧头像文件失败", data: "oldAvatar"};
                    return;
                }
                ctx.body = {code: 200, message: "修改成功", data: SrcUrl};
            }
        }
    } catch (error) {
        ctx.body = {code: 500, message: "服务器端错误", data: error};
    }
};

//修改登录密码的逻辑代码
module.exports.changePassword = async (ctx) => {
    try {
        // 获取客户端数据
        let postParams = ctx.request.body;//{userId:"",password:"",newPassword:""}
        // 判断数据有效性
        if (!postParams.hasOwnProperty("userId") || postParams.userId === "" || postParams.userId === undefined) {
            ctx.body = {code: -1, message: "请输入用户账号", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("password") || postParams.password === "" || postParams.password === undefined) {
            ctx.body = {code: -2, message: "请输入用户原密码", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("newPassword") || postParams.newPassword === "" || postParams.newPassword === undefined) {
            ctx.body = {code: -3, message: "请输入用户新密码", data: ""};
            return;
        }
        if (!/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/.test(postParams.newPassword)) {
            ctx.body = {code: -4, message: "服务器端发现你的密码格式不正确", data: ""};
            return;
        }

        //判断原密码是否和之前的一样，一样的话阻止修改
        let result = await mysql.query("select * from ?? where id = ?", ["shop_admin_user", postParams.userId]);
        if (result.length <= 0) {
            ctx.body = {code: -5, message: "该用户不存在", data: ""};
            return;
        }
        // 判断原密码是否一样
        let isMatch = await BCrypt.PasswordCompare(postParams.password, result[0].password);
        if (!isMatch) {
            ctx.body = {code: -6, message: "原密码不正确", data: ""};
            return;
        }

        //对密码进行加密修改
        let newPassword = await BCrypt.SaltEncryption(postParams.newPassword);
        // 修改数据库数据
        let returnResult = await mysql.query("update ?? set password =? where id=?", ["shop_admin_user", newPassword, postParams.userId]);
        if (returnResult) {
            ctx.body = {code: 200, message: "修改成功", data: ""};
        }


    } catch (error) {

    }
};


// 获取当前用户的信息
module.exports.getCurrentInfo = async (ctx) => {
    try {
        let userInfo = {
            userId: ctx.state.user.id,
            account: ctx.state.user.account,
            nickName: ctx.state.user.nickname,
            avatar: ctx.state.user.avatarUrl,
        };
        ctx.body = {code: 200, message: "success", data: userInfo}
    } catch (error) {
        ctx.body = {
            code: 500,
            message: "服务器端错误",
            data: error
        }
    }
};


/*******************************管理普通用户的所有路由********************************/
//处理获取所有的普用户的逻辑业务
module.exports.getAllUserInfo = async (ctx) => {
    try {
        let postParams = ctx.request.body;

        let {pageNumber, pageSize, startTime, endTime, status, sex, nickName} = postParams;
        let total = await mysql.query("select count(*) from ?? ", ["shop_user_list"]);
        let start = (pageNumber - 1) * pageSize;
        let resultArray = "";
        if (startTime === "" && endTime === "" && status === "" && sex === "" && nickName === "") {
            resultArray = await mysql.query("select * from ?? limit ?,? ", ["shop_user_list", start, pageSize]);
        } else if (startTime !== "" && endTime !== "" && status === "" && sex === "" && nickName === "") {
            resultArray = await mysql.query("select * from ?? where createdAt between ? and ? limit ?,?", ["shop_user_list", startTime, endTime, start, pageSize]);
        } else if (startTime === "" && endTime === "" && status !== "" && sex === "" && nickName === "") {
            resultArray = await mysql.query("select * from ?? where status = ? limit ?,? ", ["shop_user_list", status, start, pageSize]);
        } else if (startTime === "" && endTime === "" && status === "" && sex !== "" && nickName === "") {
            resultArray = await mysql.query("select * from ?? where sex = ? limit ?,? ", ["shop_user_list", sex, start, pageSize]);
        } else if (startTime === "" && endTime === "" && status === "" && sex === "" && nickName !== "") {
            resultArray = await mysql.query("select * from ?? where nickName =? limit ?,? ", ["shop_user_list", nickName, start, pageSize]);
        }

        //删除对应数组中的不需要的属性
        let deleteAttributeInfo = OperationalObjectives.DeleteAttribute(resultArray, ["password", "payPassword", ""]);

        ctx.body = {
            code: 200,
            message: "success",
            data: {resultArray: deleteAttributeInfo, total: total[0]["count(*)"]}
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//处理重置密码的逻辑业务
module.exports.resetPassword = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        // 加盐加密
        let password = await BCrypt.SaltEncryption("abcd1234");

        let result = await mysql.query("update ?? set password = ? where id = ?", ["shop_user_list", password, postParams.id]);
        if (result) {
            ctx.body = {code: 200, message: "密码已经重置为:abcd1234", data: ""};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//处理用户的状态的改变
module.exports.changeUserStatus = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        if (postParams.id === "" || postParams.status === "") {
            ctx.body = {code: -1, message: "参数为空不能改变用户的状态", data: ""};
            return;
        }

        let sql = "update ?? set status = ? where id = ?";
        let result = await mysql.query(sql, ["shop_user_list", postParams.status, postParams.id]);
        if (result) {
            ctx.body = {code: 200, message: "success", data: ""};
        } else {
            ctx.body = {code: -2, message: "修改失败", data: ""};
        }

    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//获取用户的收货地址
module.exports.getUserDeliveryAddress = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        if (postParams.userId === "" || postParams.userId === undefined) {
            ctx.body = {code: -1, message: "参数为空不能获取用户的收货地址信息", data: ""};
            return;
        }
        let sql = "select id,deliveryName,deliveryMobile,province,city,county,addressDetail,isDefault,createdAt,updatedAt from ?? where userId = ?";
        let addressInfo = await mysql.query(sql, ["shop_user_delivery", postParams.userId]);
        if (addressInfo.length <= 0) {
            ctx.body = {code: 200, message: "记录为空", data: []};
            return;
        }
        let returnData = [];
        for (let i = 0; i < addressInfo.length; i++) {
            let obj = {};
            obj.id = addressInfo[i].id;
            obj.deliveryName = addressInfo[i].deliveryName;
            obj.deliveryMobile = addressInfo[i].deliveryMobile;
            obj.addressInfo = addressInfo[i].province + " " + addressInfo[i].city + " " + addressInfo[i].county + " " + addressInfo[i].addressDetail;
            obj.isDefault = parseInt(addressInfo[i].isDefault);
            obj.createdAt = addressInfo[i].createdAt;
            obj.updatedAt = addressInfo[i].updatedAt;
            returnData.push(obj);
        }
        ctx.body = {code: 200, message: "success", data: returnData};

    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: ""};
    }
};

//实现用户的删除
module.exports.deleteUser = async (ctx) => {
    try {
        // 获取所有的用户id
        let ids = ctx.request.body.ids;

        //检查数据的长度
        if (ids.length <= 0) {
            ctx.body = {code: -1, message: "请选择要删除的用户", data: ""};
            return;
        }
        //检查用户是否在数据库中
        let check = [];
        for (let i = 0; i < ids.length; i++) {
            let result = await mysql.query("select * from ?? where id = ?", ["shop_user_list", ids[i]]);
            if (result.length > 0) {
                check.push(i);
            }
        }
        if (check.length !== ids.length) {
            ctx.body = {code: -2, message: "你选择的用户可能不存在", data: ""};
            return;
        }

        let deleteInfo = [];
        //循环删除数据
        for (let i = 0; i < ids.length; i++) {
            let info = await mysql.query("delete from ?? where id = ?", ["shop_user_list", ids[i]]);
            deleteInfo.push(info);
        }
        if (deleteInfo.length === ids.length) {
            ctx.body = {code: 200, message: "success", data: ""};
        }
    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//实现用户的添加
module.exports.addUser = async (ctx) => {
    try {
        // 获取用户参数
        let postParams = ctx.request.body;
        if (postParams.nickName === "") {
            ctx.body = {code: -1, message: "昵称不能为空", data: ""};
            return;
        }
        if (postParams.account === "") {
            ctx.body = {code: -1, message: "账号不能为空", data: ""};
            return;
        }
        if (postParams.email === "") {
            ctx.body = {code: -1, message: "邮箱不能为空", data: ""};
            return;
        }
        if (postParams.password === "") {
            ctx.body = {code: -1, message: "初始密码不能为空", data: ""};
            return;
        }

        //1.对数据进行处理
        // 1-1添加处理用户的性别
        let sex = postParams.sex === "男" ? 1 : 0;
        //1-2处理添加的用户的初始密码（加盐加密）
        let password = await BCrypt.SaltEncryption(postParams.password);
        //1-3处理用户的头像(默认头像地址)
        let avatarUrl = Config.baseURL + ":" + Config.port + "/images/default/default.jpg";
        //1-4处理用户的状态
        let status = postParams.status ? 0 : 1;

        // 检查数据中是否有相同的账户存在
        let hasUser = await mysql.query("select * from ?? where account = ?", ["shop_user_list", postParams.account]);

        if (hasUser.length > 0) {
            ctx.body = {code: -2, message: "该用户已经注册了，请重新选择账号", data: ""};
            return;
        }

        //需要添加到数据的数据
        let insertData = [null, postParams.nickName, postParams.account, sex, status, postParams.email, password, null, avatarUrl, new Date().toLocaleString(), null];
        let result = await mysql.query("insert into ?? values ?", ["shop_user_list", [insertData]]);
        if (result) {
            ctx.body = {code: 200, message: "success", data: ""};
        }

    } catch (e) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//实现用户的信息的批量导出
module.exports.downloadExcel = async (ctx) => {
    try {
        const _headers = ['编号', "昵称", "账号", "性别", "状态", "邮箱", "图片地址"];

        // 获取表格数据
        let result = await mysql.query("Select id,nickName,account,sex,status,email,avatarUrl from ??", ["shop_user_list"]);
        // 修改每个对象的值
        for (let i = 0; i < result.length; i++) {
            result[i].sex = result[i].sex === "1" ? "男" : "女";
            result[i].status = result[i].status === 0 ? "正常" : "封禁";
        }

        let data = TraversingArray.ObjectArrayConvertingToTwoDimension(result);
        data.unshift(_headers);

        let buffer = NXlSX.build([{name: "sheetName", data: data}]);
        ctx.set("Content-Disposition", "attachment;filename=download.xls");
        // 返回buffer流到前端
        ctx.body = buffer;
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务器端错误", data: ""};
    }
};