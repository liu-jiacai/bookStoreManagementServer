//引入数据库模块
const mysql = require("./OperateDB/mysql.js");

//获取所有的反馈信息
module.exports.getFeedBackRecordList = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        let {pageNumber, pageSize, startTime, endTime, type, userId} = postParams;
        let total = await mysql.query("select count(*) from ?? ", ["shop_user_feedback"]);
        let start = (pageNumber - 1) * pageSize;
        let resultArray = "";
        if (startTime === "" && endTime === "" && type === "" && userId === "") {
            resultArray = await mysql.query("select * from ?? limit ?,? ", ["shop_user_feedback", start, pageSize]);
        } else if (startTime !== "" && endTime !== "" && type === "" && userId === "") {
            resultArray = await mysql.query("select * from ?? where createdAt between ? and ? limit ?,?", ["shop_user_feedback", startTime, endTime, start, pageSize]);
        } else if (startTime === "" && endTime === "" && type !== "" && userId === "") {
            resultArray = await mysql.query("select * from ?? where type = ? limit ?,? ", ["shop_user_feedback", type, start, pageSize]);
        } else if (startTime === "" && endTime === "" && type === "" && userId !== "") {
            resultArray = await mysql.query("select * from ?? where userId =? ", ["shop_user_feedback", userId]);
        }

        //数据为空的时候
        if (resultArray.length <= 0) {
            ctx.body = {code: -1, message: "获取数据为空", data: ""};
            return;
        }

        let result = "";
        //获取对用反馈的的用户账号
        for (let i = 0; i < resultArray.length; i++) {
            let result1 = await mysql.query("select * from ?? where id = ?", ["shop_user_feedback", resultArray[i]["id"]]);
            if (!result1) {
                result = "服务端操作数据库失败";
                return;
            }
            //查询用户数据库实现数据的修改
            let result2 = await mysql.query("select account from ?? where id = ?", ["shop_user_list", result1[0]["userId"]]);
            if (!result2) {
                result = "服务端操作数据库失败";
                return;
            }

            //添加到原来的数据
            resultArray[i]["account"] = result2[0]["account"];
            delete resultArray[i]["userId"];

        }
        if (result === "") {
            ctx.body = {code: 200, message: "success", data: {result: resultArray, total: total[0]["count(*)"]}};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//处理反馈信息
module.exports.handleFeedBack = async (ctx) => {
    try {
        let {ids} = ctx.request.body;

        //判断是数据是否为空
        if (ids.length <= 0) {
            ctx.body = {code: -1, message: "请选择处理的记录", data: ""};
            return;
        }

        //检查数据是否存在
        let check = [];
        for (let i = 0; i < ids.length; i++) {
            let result = await mysql.query("select * from ?? where id = ?", ["shop_user_feedback", ids[i]]);
            if (!result) {
                break;
            }
            check.push(i);
        }
        if (ids.length !== check.length) {
            ctx.body = {code: -2, message: "选择的记录可有记录不存在数据库", data: ""};
            return;
        }

        //修改数据
        let updateInfo = "";

        for (let i = 0; i < ids.length; i++) {
            let result = await mysql.query("update  ?? set remark = ?,deltimeAt=? where id = ?", ["shop_user_feedback", 1, new Date().toLocaleString(), ids[i]]);
            if (!result) {
                updateInfo = "修改失败";
                break;
            }
            updateInfo = "修改成功"
        }

        if (updateInfo === "修改成功") {
            ctx.body = {code: 200, message: "success", data: ""};
        } else {
            ctx.body = {code: -3, message: "处理结果提交失败", data: ""};
        }

    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//删除反馈记录
module.exports.deleteFeedBackRecord = async (ctx) => {
    try {
        let {ids} = ctx.request.body;

        //价差数据是否为空
        if (ids.length <= 0) {
            ctx.body = {code: -1, message: "请选择要删除的记录", data: ""};
            return;
        }
        //检查数据是否存在
        let check = [];
        for (let i = 0; i < ids.length; i++) {
            let result = await mysql.query("select * from ?? where id = ?", ["shop_user_feedback", ids[i]]);
            if (!result) {
                break;
            }
            check.push(i);
        }
        //判断检查结果
        if (ids.length !== check.length) {
            ctx.body = {code: -2, message: "选择的记录可有记录不存在数据库", data: ""};
            return;
        }

        //修改数据
        let deleteInfo = "";
        for (let i = 0; i < ids.length; i++) {
            let result = await mysql.query("delete from ?? where id = ?", ["shop_user_feedback", ids[i]]);
            if (!result) {
                deleteInfo = "修改失败";
                break;
            }
            deleteInfo = "删除成功"
        }

        //返回结果
        if (deleteInfo === "删除成功") {
            ctx.body = {code: 200, message: "success", data: ""};
        } else {
            ctx.body = {code: -3, message: "删除失败", data: ""};
        }

    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};
