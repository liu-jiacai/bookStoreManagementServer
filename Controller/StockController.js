//引入数据库模块
const mysql = require("./OperateDB/mysql.js");
//操作文件模块
const DocumentFunction = require("./Function/DocumentFunction.js");
//加载path模块
const path = require("path");

//加载配置文件
const Config = require("../config/config.js");

//获取书籍的进货数据
module.exports.getStockRecordList = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        let {pageNumber, pageSize, startTime, endTime, type, bookName} = postParams;
        let total = await mysql.query("select count(*) from ?? ", ["shop_stock_record"]);
        let start = (pageNumber - 1) * pageSize;
        let resultArray = "";
        if (startTime === "" && endTime === "" && type === "" && bookName === "") {
            resultArray = await mysql.query("select * from ?? limit ?,? ", ["shop_stock_record", start, pageSize]);
        } else if (startTime !== "" && endTime !== "" && type === "" && bookName === "") {
            resultArray = await mysql.query("select * from ?? where createdAt between ? and ? limit ?,?", ["shop_stock_record", startTime, endTime, start, pageSize]);
        } else if (startTime === "" && endTime === "" && type !== "" && bookName === "") {
            resultArray = await mysql.query("select * from ?? where type = ? limit ?,? ", ["shop_stock_record", type, start, pageSize]);
        } else if (startTime === "" && endTime === "" && type === "" && bookName !== "") {
            resultArray = await mysql.query("select * from ?? where bookName =? ", ["shop_stock_record", bookName]);
        }
        ctx.body = {
            code: 200,
            message: "success",
            data: {resultArray: resultArray, total: total[0]["count(*)"]}
        };
    } catch
        (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};


//获取书籍的部分信息
module.exports.getBookPartInfo = async (ctx) => {
    try {
        let sql = "select id,name,stock from ?? ";
        let result = await mysql.query(sql, ["shop_book_list"]);
        if (result.length <= 0) {
            ctx.body = {code: 200, message: "empty", data: []};
        } else {
            ctx.body = {code: 200, message: "success", data: result}
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务器端错误", data: e}
    }
};

//添加进货数据
module.exports.addIncomingData = async (ctx) => {
    //获取客户端参数
    let params = ctx.request.body;
    let files = ctx.request.files;

    try {
        if (params["stockType"] === "") {
            ctx.body = {code: -1, message: "请选择增加的类型", data: ""};
            return;
        }
        //修改库存
        if (params["stockType"] === 1) {
            if (params.bookId === "") {
                ctx.body = {code: -2, message: "请选择书籍", data: ""};
                return;
            }
            //修改数据库数据
            // 1-1查询数库中是否有该记录
            let isHave = await mysql.query("select * from ?? where id = ?", ["shop_book_list", params.bookId]);
            if (isHave.length <= 0) {
                ctx.body = {code: -3, message: "没有找到该书无法修改", data: ""};
                return;
            }

            //修改数据库该条数据的内容
            let result = await mysql.query("update ?? set stock=? where id = ?", ["shop_book_list", params.stock, params.bookId]);
            if (!result) {
                ctx.body = {code: -4, message: "记录修改失败", data: ""};
                return;
            }
            //构建新的数据
            let insertData = [null, params.bookId, params["bookName"], params.stock - isHave[0].stock, isHave[0]["stockPrice"], 1, params["remark"], new Date().toLocaleString()];
            let insertResult = await mysql.query("insert into ?? values ?", ["shop_stock_record", [insertData]]);
            if (!insertResult) {
                ctx.body = {code: -5, message: "进库记录添加失败", data: ""};
            } else {
                ctx.body = {code: 200, message: "添加成功", data: ""};
            }
        } else {
            //新书入库
            if (files.bookImage.name === "") {
                let deleteInfo = await DocumentFunction.deleteFile(files.bookImage.path);
                if (deleteInfo === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                ctx.body = {code: -2, message: "请上传文件", data: ""};
                return;
            }
            //检查文件的类型
            if (!/.*\.(png|jpg|JPG|PNG|JPEG)$/.test(files.bookImage.name)) {
                let deleteInfo = await DocumentFunction.deleteFile(files.bookImage.path);
                if (deleteInfo === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                ctx.body = {code: -3, message: "上传图片文件类型错误", data: ""};
                return;
            }
            // 文件大小
            if (files.bookImage.size > 1024 * 500) {
                let deleteInfo = await DocumentFunction.deleteFile(files.bookImage.path);
                if (deleteInfo === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                ctx.body = {code: -4, message: "上传图片文件大小不能超过500KB", data: ""};
                return;
            }
            //检查其他字段是否有数据
            if (params["bookName"] === "" || params["author"] === "" || params.title === "") {
                let deleteInfo = await DocumentFunction.deleteFile(files.bookImage.path);
                if (deleteInfo === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                ctx.body = {code: -5, message: "上传数据不完整", data: ""};
                return;
            }
            //创建文件名
            let filename = DocumentFunction.createFileName(new Date()) + path.extname(files.bookImage.name);
            let imageUrl = Config.baseURL + "/images/book/" + filename;

            //重命名文件
            let renameFileInfo = await DocumentFunction.renameFile(files.bookImage.path, path.join(__dirname, "../public/images/book/", filename));
            if (renameFileInfo === -1) {
                let deleteInfo = await DocumentFunction.deleteFile(files.bookImage.path);
                if (deleteInfo === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                return;
            }

            //构建新的数据记录
            let insertData1 = [null, params["bookName"], params["author"], params["press"], params["classifyId"], params.title, params["bookDesc"], params["stockNum"], params["stockPrice"], 0, 0, 0, imageUrl, new Date().toLocaleString(), null];
            let insertResult1 = await mysql.query("insert into ?? values ?", ["shop_book_list", [insertData1]]);

            if (!insertResult1) {
                let deleteInfo = await DocumentFunction.deleteFile(files.bookImage.path);
                if (deleteInfo === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                ctx.body = {code: -6, message: "上传图片文件大小不能超过500KB", data: ""};
                return;
            }

            //构建新的数据记录
            let insertData2 = [null, insertResult1["insertId"], params["bookName"], params["stockNum"], params["stockNum"], 0, params["desc"], new Date().toLocaleString()];
            let insertResult2 = await mysql.query("insert into ?? values ?", ["shop_stock_record", [insertData2]]);
            if (!insertResult2) {
                //首次文件存放的位置删除
                let deleteInfo = await DocumentFunction.deleteFile(files.bookImage.path);
                if (deleteInfo === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                // 重名文件夹后的位置
                let deleteInfo2 = await DocumentFunction.deleteFile(path.join(__dirname, "public/images/book/", filename));
                if (deleteInfo2 === -1) {
                    ctx.body = {code: -1, message: "服务器临时文件删除失败", data: ""};
                }
                ctx.body = {code: -7, message: "增加进货记录失败", data: ""};
            } else {
                ctx.body = {code: 200, message: "success", data: ""};
            }
        }
    } catch (e) {
        console.log(e);
        await DocumentFunction.deleteFile(files.bookImage.path);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//删除进货记录
module.exports.deleteStockRecord = async (ctx) => {
    try {
        //客户端数据
        let postParams = ctx.request.body;
        //判断有效数据
        if (postParams["ids"].length <= 0) {
            //返回数据
            ctx.body = {code: -1, message: "请选择要删除的记录", data: ""};
            return;
        }
        //先检查数据是否存在存在再删除
        let check = [];
        for (let i = 0; i < postParams["ids"].length; i++) {
            let result = await mysql.query("select * from ?? where id = ?", ["shop_stock_record", postParams["ids"][i]]);
            if (result) {
                check.push(i);
            } else {
                break;
            }
        }
        if (check.length !== postParams["ids"].length) {
            ctx.body = {code: -2, message: "删除记录中有记录在数据库中不存在", data: ""};
            return;
        }

        //删除记录
        let deleteInfo = "";
        for (let i = 0; i < postParams["ids"].length; i++) {
            let result = await mysql.query("delete from ?? where id = ?", ["shop_stock_record", postParams["ids"][i]]);
            if (!result) {
                deleteInfo = "删除失败";
            }
        }
        if (deleteInfo === "") {
            ctx.body = {code: 200, message: "success", data: ""};
        }

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};