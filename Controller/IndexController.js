//引入模块
const mysql = require("../Controller/OperateDB/mysql.js");
const AccessTime = require("./Function/AccessTime.js");
const TraversingData = require("./Function/TraversingArray.js");

//获取订单的消息
module.exports.getOrderStatistics = async (ctx) => {
    try {
        // 获取相关数据(当天之内的所有订单)
        let timeStart = new Date(new Date(new Date().getTime()).setHours(0, 0, 0, 0));
        let endTime = new Date(new Date(new Date().getTime()).setHours(23, 59, 59, 999));
        let result = await mysql.query("select count(*) from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", timeStart, endTime]);

        //获取相关未处理的订单数据 （表示的是已付款没有安排发货的订单）
        let result2 = await mysql.query("select count(*) from ?? where status = 1", ["shop_order_list"]);

        //获取相关退款的订单数据
        let result3 = await mysql.query("select count(*) from ?? where status = 6", ["shop_order_list"]);

        let returnResult = {
            todayOrderNum: result[0]["count(*)"],
            dealWithOrderNum: result2[0]["count(*)"],
            refundOrderNum: result3[0]["count(*)"],
        };

        ctx.body = {code: 200, message: "获取成功", data: returnResult}


    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: error}
    }

};

//获取日月周的销售量/销售额
module.exports.getOrderStatisticsByType = async (ctx) => {
    //遍历销量的函数
    async function TraversingDataOfCount(array) {
        let totalCount = 0; //总数
        if (array.length <= 0) {
            ctx.body = {code: -1, message: " 记录为空", data: ""};
            return totalCount;
        } else {
            //循环判断每一条记录
            for (let i = 0; i < array.length; i++) {
                totalCount += array[i].count;
            }
            return totalCount;
        }
    }

    //遍历销售额的函数
    async function TraversingDataOfLimit(array) {
        let totalMoney = 0;
        if (array.length <= 0) {
            ctx.body = {code: -1, message: " 记录为空", data: ""};
            return totalMoney;
        }

        for (let i = 0; i < array.length; i++) {
            totalMoney += array[i].freight + array[i].shopMoney;
        }

        return totalMoney;
    }

    try {
        //客户端参数
        let postParams = ctx.request.body;
        if (postParams.type === "" || postParams.type === undefined) {
            ctx.body = {code: -2, message: "类型不能为空", data: ""};
            return;
        }
        // 判断的是销量还是销售额 1 表示销售量 2 表示销售额
        if (postParams.type === 0) {
            // 获取相关数据(当天之内的所有订单)当日销量
            let DateRange = AccessTime.theWholeDay(new Date());
            let DailySales = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", DateRange.timeStart, DateRange.endTime]);
            let dayCount = await TraversingDataOfCount(DailySales);

            // 当周销量
            let WeekRange = AccessTime.currentWeek(new Date());
            let WeeklySales = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", WeekRange.timeStart, WeekRange.endTime]);
            let weekCount = await TraversingDataOfCount(WeeklySales);

            // 当月销量
            let MonthRange = AccessTime.currentMonth(new Date());
            let MonthlySales = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", MonthRange.timeStart, MonthRange.endTime]);
            let monthCount = await TraversingDataOfCount(MonthlySales);
            //构建返回对象
            let returnData = {today: dayCount, week: weekCount, month: monthCount};

            ctx.body = {
                code: 200,
                message: "success",
                data: returnData
            }
        } else {
            //获取日销售额
            let DateRange = AccessTime.theWholeDay(new Date());
            let DailySales = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", DateRange.timeStart, DateRange.endTime]);
            let DailyTotal = await TraversingDataOfLimit(DailySales);

            //获取周销售额
            let weekRange = AccessTime.currentWeek(new Date());
            let WeeklySales = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", weekRange.timeStart, weekRange.endTime]);
            let weekTotal = await TraversingDataOfLimit(WeeklySales);

            //获取月销售额
            let monthRange = AccessTime.currentMonth(new Date());
            let MonthSales = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", monthRange.timeStart, monthRange.endTime]);
            let MonthTotal = await TraversingDataOfLimit(MonthSales);

            //构建返回对象
            let returnData = {today: DailyTotal, week: weekTotal, month: MonthTotal};

            ctx.body = {
                code: 200,
                message: "success",
                data: returnData
            }
        }
    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: error}
    }
};

//获取七个月的销售量趋势变化,销售额的趋势变化
module.exports.getTrendInfoByType = async (ctx) => {

    //对数数据进行分组
    async function DataGroupOfMonth(array) {
        // 对数据进行判断分类
        let nowMonth = new Date().getMonth() + 1;
        let arr1 = [];
        let arr2 = [];
        let arr3 = [];
        let arr4 = [];
        let arr5 = [];
        let arr6 = [];
        let arr7 = [];

        for (let i = 0; i < array.length; i++) {
            if (array[i].createdAt.getMonth() + 1 === nowMonth) {
                arr1.push(array[i]);
            } else if (array[i].createdAt.getMonth() + 1 === nowMonth - 1) {
                arr2.push(array[i]);
            } else if (array[i].createdAt.getMonth() + 1 === nowMonth - 2) {
                arr3.push(array[i]);
            } else if (array[i].createdAt.getMonth() + 1 === nowMonth - 3) {
                arr4.push(array[i]);
            } else if (array[i].createdAt.getMonth() + 1 === nowMonth - 4) {
                arr5.push(array[i]);
            } else if (array[i].createdAt.getMonth() + 1 === nowMonth - 5) {
                arr6.push(array[i]);
            } else {
                arr7.push(array[i]);
            }
        }
        return {arr1, arr2, arr3, arr4, arr5, arr6, arr7};
    }

    //遍历销量的函数
    async function TraversingDataOfCount(array) {
        let totalCount = 0; //总数
        if (array.length <= 0) {
            ctx.body = {code: -1, message: " 记录为空", data: ""};
            return totalCount;
        } else {
            //循环判断每一条记录
            for (let i = 0; i < array.length; i++) {
                totalCount += array[i].count;
            }
            return totalCount;
        }
    }

    //遍历销售额的函数
    async function TraversingDataOfLimit(array) {
        let totalMoney = 0;
        if (array.length <= 0) {
            ctx.body = {code: -1, message: " 记录为空", data: ""};
            return totalMoney;
        }

        for (let i = 0; i < array.length; i++) {
            totalMoney += array[i].freight + array[i].shopMoney;
        }

        return totalMoney;
    }

    try {
        //获取客户端数据
        let postParams = ctx.request.body;
        if (postParams.type === "" || postParams.type === undefined) {
            ctx.body = {code: -1, message: "类型不能为空", data: ""};
            return;
        }

        if (postParams.type === 0) {
            // 时间范围
            let MonthRange = AccessTime.MonthQuantum(new Date());
            //查询数据库(时间段内所有数据)
            let resultArray = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", MonthRange.startTime, MonthRange.endTime])

            let DataGroupResult = await DataGroupOfMonth(resultArray);
            let arr1Count = await TraversingDataOfCount(DataGroupResult.arr1); //例如 ：9
            let arr2Count = await TraversingDataOfCount(DataGroupResult.arr2); //8
            let arr3Count = await TraversingDataOfCount(DataGroupResult.arr3); //7
            let arr4Count = await TraversingDataOfCount(DataGroupResult.arr4);//6
            let arr5Count = await TraversingDataOfCount(DataGroupResult.arr5);//5
            let arr6Count = await TraversingDataOfCount(DataGroupResult.arr6);//4
            let arr7Count = await TraversingDataOfCount(DataGroupResult.arr7);//3

            //返回的数据
            let returnData = [arr7Count, arr6Count, arr5Count, arr4Count, arr3Count, arr2Count, arr1Count];


            ctx.body = {code: 200, message: "success", data: {Month: MonthRange.endTime.getMonth() + 1, returnData}};
        } else {
            // 时间范围
            let MonthRange = AccessTime.MonthQuantum(new Date());
            //查询数据库(时间段内所有数据)
            let resultArray = await mysql.query("select * from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", MonthRange.startTime, MonthRange.endTime])

            let DataGroupResult = await DataGroupOfMonth(resultArray);
            let arr1Limit = await TraversingDataOfLimit(DataGroupResult.arr1); //例如 ：9
            let arr2Limit = await TraversingDataOfLimit(DataGroupResult.arr2); //8
            let arr3Limit = await TraversingDataOfLimit(DataGroupResult.arr3); //7
            let arr4Limit = await TraversingDataOfLimit(DataGroupResult.arr4);//6
            let arr5Limit = await TraversingDataOfLimit(DataGroupResult.arr5);//5
            let arr6Limit = await TraversingDataOfLimit(DataGroupResult.arr6);//4
            let arr7Limit = await TraversingDataOfLimit(DataGroupResult.arr7);//3

            //返回的数据
            let returnData = [arr7Limit, arr6Limit, arr5Limit, arr4Limit, arr3Limit, arr2Limit, arr1Limit];

            ctx.body = {code: 200, message: "success", data: {Month: MonthRange.endTime.getMonth() + 1, returnData}};
        }
    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务器端错误", data: error};
    }
};


//获取前10数据
module.exports.getTop10Info = async (ctx) => {

    //获取在本月内所有销量的数据(并获取每个商品对应的name)
    async function TraversingArray(array) {
        let data = [];
        // 传进来的数组长度为0 返回空数组
        if (array.length <= 0) {
            return [];
        }

        //循环array
        for (let i = array.length - 1; i >= 0; i--) {
            if (array[i].hasOwnProperty("bookId")) {
                let res = await mysql.query("select id,name from ?? where id=?", ["shop_book_list", array[i].bookId]);
                //构建一个新对象
                let obj = {
                    bookId: res[0]["id"],
                    name: res[0]["name"],
                    count: array[i].count
                };
                data.push(obj);
            }
        }
        //数组合并
        return data;
    }

    try {
        // 本月时间
        let MontRange = AccessTime.currentMonth(new Date());

        //获取销量前十 （获取所有的记录的前十）
        let sql = "select bookId,count from ?? where createdAt BETWEEN ? and ? ";
        let salesResult = await mysql.query(sql, ["shop_order_list", MontRange.timeStart, MontRange.endTime]);

        //如果结构返回的数据是空数组那么data就为[]
        let data = await TraversingArray(salesResult);

        //合并数组中相同的数据
        let mergeSales = TraversingData.mergeArray(data, "bookId", "name", "count");
        //对数组排序
        let sortSalesData = TraversingData.sortArray(mergeSales, "count");

        let top10Sales = [];
        //判断数组的长度
        if (sortSalesData.length < 10) {
            top10Sales = TraversingData.fillingData(sortSalesData, {bookId: "", name: "", count: ""});
        } else {
            top10Sales = sortSalesData.slice(0, 9);
        }

        //获取进货前十
        let resultStock = await mysql.query("select name,stock from ?? where createdAt BETWEEN ? and ? ", ["shop_book_list", MontRange.timeStart, MontRange.endTime]);
        //排序数组
        let sortStockData = TraversingData.sortArray(resultStock, "stock");
        //前十数据
        let top10Stock = [];
        // //判断数组的长度
        if (sortStockData.length < 10) {
            top10Stock = TraversingData.fillingData(sortStockData, {name: "", stock: ""});
        } else {
            top10Stock = sortStockData.slice(0, 9);
        }


        //获取销售额前十
        let resultUser = await mysql.query("select userId,shopMoney,freight from ?? where createdAt BETWEEN ? and ?", ["shop_order_list", MontRange.timeStart, MontRange.endTime]);
        //合并数组
        let mergeUserData = TraversingData.mergeArray2(resultUser, "userId", "freight", "shopMoney");

        //填充数组
        let top10User = [];
        let sortUserData = [];

        if (mergeUserData.length > 0) {
            //根据userId进行相关的信息获取
            for (let i = 0; i < mergeUserData.length; i++) {
                let result = await mysql.query("select account from ?? where id = ?", ["shop_user_list", mergeUserData[i].userId]);
                mergeUserData[i].account = result[i].account;
            }
            sortUserData = TraversingData.sortArray(mergeUserData, "shopMoney");
        }

        if (sortUserData.length < 10) {
            top10User = TraversingData.fillingData(sortUserData, {userId: "", account: "", shopMoney: ""});
        } else {
            top10User = sortUserData.slice(0, 9);
        }


        ctx.body = {code: 200, message: "success", data: {top10Sales, top10Stock, top10User}};

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务器端错误", data: error}
    }
};