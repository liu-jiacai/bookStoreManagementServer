// 数据库模块
const mysql = require("./OperateDB/mysql.js");
const OperationalObjectives = require("./Function/OperationalObjectives.js");
const TraversingArray = require("./Function/TraversingArray.js");
const areaInfo = require("./Function/AreaInfo.js");
const Config = require("../config/config.js");
const NXlSX = require("node-xlsx");
const fs = require("fs");
const path = require("path");
const ProcessingPhotoAddresses = require("./Function/ProcessingPhotoAddresses.js");
const DocumentFunction = require("./Function/DocumentFunction.js")

//获取所有的订单数据
module.exports.getAllOrderList = async (ctx) => {

    async function getArrayOfOrder(sql, params) {
        let array = await mysql.query(sql, params);
        if (!array) {
            return [];
        }
        return array;
    }

    try {
        let postParams = ctx.request.body;
        let {orderId, status, startTime, endTime, pageNumber, pageSize} = postParams;
        // 进行数据获取的判断
        let orderArray = "";
        pageSize = parseInt(pageSize);
        let start = (pageNumber - 1) * pageSize;
        if (orderId === "" && status === "" && startTime === "" && endTime === "") {
            // 情况一 没有任何参数
            let sql = "select * from ??  limit ?,?";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", start, pageSize]);
        } else if (orderId !== "" && status === "" && startTime === "" && endTime === "") {
            //情况二 通过 orderId来获取
            let sql = "select * from ?? where orderId = ?";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", orderId]);

        } else if (orderId === "" && status !== "" && startTime === "" && endTime === "") {
            // 情况三（只能通过status来获取数据）
            let sql = "select * from ?? where status = ? limit ?,?";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", status, start, pageSize]);

        } else if (orderId === "" && status === "" && startTime !== "" && endTime !== "") {
            // 情况四（只能通过 时间段 来获取数据）
            let sql = "select * from ?? where createdAt between ? and ? ";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", startTime, endTime]);

        } else if (orderId !== "" && status !== "" && startTime === "" && endTime === "") {
            // 情况五（只能通过status和订单号 orderId来获取来获取数据）
            let sql = "select * from ?? where status = ? and orderId= ? ";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", status, orderId]);
        } else if (orderId !== "" && status === "" && startTime !== "" && endTime !== "") {
            // 情况六 在某个时间范围内搜索某个orderId
            let sql = "select * from ?? where  orderId= ?  and createdAt between ? and ? ";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", orderId, startTime, endTime]);


        } else if (orderId === "" && status !== "" && startTime !== "" && endTime !== "") {
            // 情况七 在某个时间范围内搜索某个
            let sql = "select * from ?? where  status= ?  and createdAt between ? and ? limit ?,?";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", orderId, startTime, endTime, start, pageSize]);

        } else if (orderId !== "" && status !== "" && startTime !== "" && endTime !== "") {
            // 情况八 在某个时间范围内搜索某个
            let sql = "select * from ?? where  orderId= ? and status=? and createdAt between ? and ? ";
            orderArray = await getArrayOfOrder(sql, ["shop_order_list", orderId, status, startTime, endTime]);
        }

        //获取所有数据
        if (orderArray.length <= 0) {
            ctx.body = {code: -1, message: "获取数据失败", data: ""};
            return;
        }

        //删除对象数组中对象的属性
        let orderResults = OperationalObjectives.DeleteAttribute(orderArray, ["userName"]);


        //循环数据数据（根据用户id获取用户的账号，并删除对象中的userId属性并设置每个对象的totalMoney）
        for (let i = 0; i < orderResults.length; i++) {
            let user = await mysql.query("select account from ?? where id = ?", ["shop_user_list", orderResults[i].userId]);
            let book = await mysql.query("select name,price,salePrice,imageUrl from ?? where id = ?", ["shop_book_list", orderResults[i].bookId]);


            let refundResult = await mysql.query("select remark from ?? where refundId = ?", ["shop_refund_record", orderResults[i]["refundId"]]);


            orderResults[i].account = user[0].account; //添加对象
            orderResults[i].totalMoney = orderResults[i].freight + orderResults[i].shopMoney;

            if (refundResult.length > 0) {
                orderResults[i].refundRemark = refundResult[0]["remark"];
            } else {
                orderResults[i].refundRemark = null;
            }

            delete orderResults[i]["refundId"];
            //获取书籍信息
            orderResults[i].bookInfo = {
                name: book[0].name,
                price: book[0].price,
                salePrice: book[0].salePrice,
                imageUrl: Config.baseURL + ":" + Config.port + book[0].imageUrl.toString().substring(16)
            }
        }

        let deliveryResults = await mysql.query("select * from ??", ["shop_delivery_company"]);

        deliveryResults.unshift({id: 0, name: "请选择快递公司"});

        let total = await mysql.query("select count(*) from ??", ["shop_order_list"]);
        ctx.body = {code: 200, message: "success", data: {orderResults, deliveryResults, total: total[0]["count(*)"]}};

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

// 实现快递公司的修改和快递单号的修改
module.exports.editDeliveryInfo = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        // 检查数据有效性
        if (!postParams.hasOwnProperty("orderId") || postParams.orderId === "" || postParams.orderId === undefined) {
            ctx.body = {code: -1, message: "请上传订单号", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("deliveryId") || postParams.deliveryId === "" || postParams.deliveryId === undefined) {
            ctx.body = {code: -2, message: "快递公司编号", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("courierNumber") || postParams.courierNumber === "" || postParams.courierNumber === undefined) {
            ctx.body = {code: -3, message: "请填写快递单号", data: ""};
            return;
        }

        //检查订单号
        let resultArray = await mysql.query("select orderId from ?? where orderId = ?", ["shop_order_list", postParams.orderId]);
        if (!resultArray || resultArray.length <= 0) {
            ctx.body = {code: -4, message: "该订单号不存在，无法修改", data: ""};
            return;
        }
        if (!/^[A-Z]{2}[0-9]{16}$/.test(postParams.courierNumber)) {
            ctx.body = {code: -5, message: "输入的订单号格式不正确", data: ""};
            return;
        }

        //修改数库
        let sql = "update  ?? set deliveryId=?,courierNumber=? where orderId = ?";
        let result = await mysql.query(sql, ["shop_order_list", postParams["deliveryId"], postParams.courierNumber, postParams.orderId]);
        if (!result) {
            ctx.body = {code: -6, message: "提交失败", data: ""}
        } else {
            ctx.body = {code: 200, message: "修改成功", data: ""}
        }
    } catch (error) {
        console.log(error);
        ctx.body = {code: 200, message: "服务端错误", data: error}
    }
};

//获取当前订单的物流信息
module.exports.getLogisticsInfo = async (ctx) => {
    try {
        let postParams = ctx.request.body;

        //检查数据的有效性
        if (!postParams.hasOwnProperty("orderId") || postParams.orderId === "" || postParams.orderId === undefined) {
            ctx.body = {code: -1, message: "获取数据的参数不完整", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("deliveryAddressId") || postParams.deliveryAddressId === "" || postParams.deliveryAddressId === undefined) {
            ctx.body = {code: -1, message: "获取数据的参数不完整", data: ""};
            return;
        }
        //获取地址
        let address = await mysql.query("select province,city,county,addressDetail from ?? where id = ?", ["shop_user_delivery", parseInt(postParams.deliveryAddressId)]);

        //创建对象
        let addressDefault = {
            content: address[0].province + " " + address[0].city + " " + address[0].county + " " + address[0]["addressDetail"],
            icon: "el-icon-suitcase",
            color: "#409EFF"
        };

        let orderInfo = await mysql.query("select status from ?? where orderId = ?", ["shop_order_list", postParams.orderId]);
        //查询数据库
        let logisticsInfo = await mysql.query("select * from ?? where orderId = ?", ["shop_logisticsInfo_list", postParams.orderId]);

        // 进行条件的判断
        if (parseInt(orderInfo[0].status) >= 2) {
            let returnData = [];
            returnData.push(addressDefault);
            //删除不必要的属性
            let deleteLogisticsInfo = OperationalObjectives.DeleteSpeciallyAttribute(logisticsInfo, "orderId");
            //对每个对象添加相应属性
            let addLogisticsInfos = OperationalObjectives.AddAttribute(deleteLogisticsInfo, ["color", "icon"], ["#409EFF", "el-icon-sunny"]);
            // 修改数组对象的指定属性
            let changeLogisticsInfos = OperationalObjectives.changeSpeciallyAttribute(addLogisticsInfos, "createAt", "timestamp");

            //对对象数组进行排序
            let sortArray = TraversingArray.sortArray(changeLogisticsInfos, "timestamp");

            // 合并数组
            returnData.push(...sortArray);

            // 构建新的每个时间段的对象
            ctx.body = {code: 200, message: "", data: returnData};
        } else {
            let returnResult = [];
            let logisticsInfo = [{
                content: '商品还物流信息',
                timestamp: new Date().toLocaleString(),
                color: "#409EFF"
            }];
            returnResult.push(addressDefault);
            returnResult.push(...logisticsInfo);
            ctx.body = {code: 200, message: "", data: returnResult}
        }
    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "", data: error}
    }
};

//添加物流动态
module.exports.addLogisticsInfo = async (ctx) => {
    try {
        let postParams = ctx.request.body;//{orderId,content}
        //判断数据有效性
        if (!postParams.hasOwnProperty("orderId") || postParams.orderId === "" || postParams.orderId === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("content") || postParams.content === "" || postParams.content === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: ""};
            return;
        }

        // 内容必须是中文
        if (!/^[:|\u4e00-\u9fa5]+$/.test(postParams.content)) {
            ctx.body = {code: -2, message: "填写的必须是中文", data: ""};
            return;
        }
        //修改数据库
        let sql = "insert into ?? values ?";
        let insertData = [postParams.orderId, postParams.content, new Date().toLocaleString()];
        let result = await mysql.query(sql, ["shop_logisticsInfo_list", [insertData]]);
        if (result) {
            ctx.body = {code: 200, message: "添加成功", data: ""};
        }

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "", data: error}
    }
};

//获取订单的收货地址
module.exports.getDeliveryAddress = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        //检查数据的有效性
        if (!postParams.hasOwnProperty("orderId") || postParams.orderId === "" || postParams.orderId === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: ""};
            return;
        }
        let deliveryAddressId = await mysql.query("select deliveryAddressId from ?? where orderId = ?", ["shop_order_list", postParams.orderId]);
        if (deliveryAddressId.length <= 0) {
            ctx.body = {code: -2, message: "该订单不存在", data: ""};
            return;
        }
        let sql = "select deliveryName,deliveryMobile,province,city,county,addressDetail from ?? where id =?";
        let address = await mysql.query(sql, ["shop_user_delivery", deliveryAddressId[0]["deliveryAddressId"]]);
        if (address) {
            ctx.body = {code: 200, message: "success", data: address[0]};
        }

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务器端错误", data: error};
    }
};

//获取用户收货地址列表
module.exports.getUserAddressList = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        if (postParams.userId === "" || postParams.userId === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: ""};
            return;
        }
        let sql = "select id, deliveryName,deliveryMobile,province,city,county,addressDetail from ?? where userId =?";
        let addressList = await mysql.query(sql, ["shop_user_delivery", postParams.userId]);
        if (addressList) {
            ctx.body = {code: 200, message: "success", data: addressList};
        } else {
            ctx.body = {code: 200, message: "fail", data: []};
        }
    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务器端错误", data: error};
    }
};

//修改用户的收货地址
module.exports.updateDeliveryAddress = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        //检查数据有效性
        if (!postParams.hasOwnProperty("orderId") || postParams.orderId === "" || postParams.orderId === undefined) {
            ctx.body = {code: -1, message: "提交的数据不完整", data: ""};
            return;
        }
        if (!postParams.hasOwnProperty("deliveryAddressId") || postParams.deliveryAddressId === "" || postParams.deliveryAddressId === undefined) {
            ctx.body = {code: -1, message: "提交的数据不完整", data: ""};
            return;
        }
        let updateInfo = await mysql.query("select * from ?? where orderId=?", ["shop_order_list", postParams.orderId]);
        if (updateInfo.length <= 0) {
            ctx.body = {code: -2, message: "该订单不存在无法修改", data: ""};
            return;
        }
        //修改数据
        let sql = "update ?? set deliveryAddressId=? where orderId = ?";
        let updateResult = await mysql.query(sql, ["shop_order_list", postParams.deliveryAddressId, postParams.orderId]);
        if (updateResult) {
            ctx.body = {code: 200, message: "修改成功", data: ""};
        }
    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务器端错误", data: error};
    }
};

//获取省市县的信息逻辑业务
module.exports.getAreaDataInfo = async (ctx) => {
    try {
        ctx.body = {
            code: 200,
            message: "success",
            data: {province: areaInfo.province, city: areaInfo.city, county: areaInfo.county}
        };
    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: ""}
    }
};

//添加收货地址
module.exports.addAddressInfo = async (ctx) => {
    try {
        //获取客户端参数
        let postParams = ctx.request.body;
        // 检查数据有效性
        if (!/^[\u4e00-\u9fa5]+$/.test(postParams.deliveryName)) {
            ctx.body = {code: -2, message: "收货人必须是中文", data: "deliveryName"};
            return;
        }
        if (!postParams.hasOwnProperty("deliveryMobile") || postParams.deliveryMobile === "" || postParams.deliveryMobile === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: "deliveryMobile"};
            return;
        }
        if (!postParams.hasOwnProperty("provinceId") || postParams.provinceId === "" || postParams.provinceId === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: "provinceId"};
            return;
        }
        if (!postParams.hasOwnProperty("cityId") || postParams.cityId === "" || postParams.cityId === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: "cityId"};
            return;
        }
        if (!postParams.hasOwnProperty("countyId") || postParams.countyId === "" || postParams.countyId === undefined) {
            ctx.body = {code: -1, message: "提交数据不完整", data: "countyId"};
            return;
        }
        if (!/^[\u4e00-\u9fa50-1A-Za-z]+$/.test(postParams["detailAddress"])) {
            ctx.body = {code: -1, message: "详细地址不能是除中文和数字以外的字符", data: "detailAddress"};
            return;
        }
        if (!/^1[3456789]\d{9}$/.test(postParams.deliveryMobile)) {
            ctx.body = {code: -3, message: "手机号格式不正确", data: "detailAddress"};
            return;
        }

        //实现根据id获取名字
        let province = areaInfo.getNameById(areaInfo.province, "provinceId", postParams.provinceId, "name");
        let city = areaInfo.getNameById(areaInfo.city, "cityId", postParams.cityId, "name");
        let county = areaInfo.getNameById(areaInfo.county, "countyId", postParams.countyId, "name");

        //插入数据库
        let sql = "insert into ?? (userId,deliveryName,deliveryMobile,province,city,county,addressDetail,isDefault,createdAt) values ?";
        let insertData = [postParams.userId, postParams.deliveryName, postParams.deliveryMobile, province, city, county, postParams["detailAddress"], 0, new Date().toLocaleString()];
        let result = await mysql.query(sql, ["shop_user_delivery", [insertData]]);

        if (result) {
            ctx.body = {code: 200, message: "success", data: ""};
        }

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//处理退款的逻辑业务
module.exports.submitRefundOrder = async (ctx) => {
    try {
        let {refundStatus, refundRemark, ids} = ctx.request.body;
        // 检查数据有效性
        if (refundStatus === "" || refundStatus === undefined) {
            ctx.body = {code: -1, message: "参数错误", data: ""};
            return;
        }
        if (refundRemark === "" || refundRemark === undefined) {
            ctx.body = {code: -2, message: "退款备注不能为空", data: ""};
            return;
        }
        if (ids.length === 0) {
            ctx.body = {code: -2, message: "请选择订单号", data: ""};
            return;
        }

        //检查对象中的订单号是否存在
        let orderIds = ids;
        let count = 0;

        //循环检查数据在数据库的存在与否
        for (let i = 0; i < orderIds.length; i++) {
            //查询数据 (检查订单是否存在)
            let orderInfo = await mysql.query("select * from ?? where orderId = ?", ["shop_order_list", orderIds[i]]);
            if (!orderInfo) {
                break;
            }
            if (orderInfo.length <= 0) {
                continue;
            }
            count++;
        }

        //判断记录条数
        if (count !== orderIds.length) {
            ctx.body = {code: -3, message: "订单不存在", data: ""};
            return;
        }

        //实现数据的修改
        let result = "";
        let status = parseInt(refundStatus) === 1 ? 7 : 8; //判断同意退款还是不同意退款

        //循环数据
        for (let i = 0; i < orderIds.length; i++) {
            //获取每个订单的退款订单号
            let orderInfo = await mysql.query("select refundId from ?? where orderId = ?", ["shop_refund_record", orderIds[i]]);
            if (!orderInfo) {
                result = "服务端数据库错误";
                break;
            }

            if (orderInfo.length > 0) {
                let sql = "update ?? set status=? where orderId = ?";
                let updateInfo = await mysql.query(sql, ["shop_order_list", status, orderIds[i]]);
                let sql2 = "update ?? set status=?,remark=? where refundId = ?";
                let updateInfo1 = await mysql.query(sql2, ["shop_refund_record", status, refundRemark, orderInfo[0]["refundId"]]);
                if (!updateInfo || !updateInfo1) {
                    result = "修改失败";
                } else {
                    result = "修改成功";
                }

            } else {
                result = "该退款订单不存在";
            }
        }

        if (result === "修改成功") {
            ctx.body = {code: 200, message: "success", data: ""}
        } else {
            ctx.body = {code: -4, message: result, data: ""}
        }
    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: error}
    }
};

//确认订单的逻辑业务
module.exports.confirmOrder = async (ctx) => {
    try {
        //客户端参数
        let {ids} = ctx.request.body;
        //检查数据的有效性
        if (ids === undefined || ids === "") {
            ctx.body = {code: -1, message: "提交数据不完整", data: ""};
            return;
        }

        //判断数据是否完成了相关步骤
        let flag = false;
        //循坏数据确定是否生成了courierNumber deliveryId
        for (let i = 0; i < ids.length; i++) {
            let result = await mysql.query("select courierNumber, deliveryId from ?? where orderId = ?", ["shop_order_list", ids[i]]);
            if (result[0]["courierNumber"] === "" || result[0]["courierNumber"] === null || result[0]["deliveryId"] === "" || result[0]["deliveryId"] === null) {
                flag = true;
                break;
            }
        }

        //判断数据
        if (flag) {
            ctx.body = {code: -2, message: "请编辑快递公司或生成快定订单号", data: ""};
            return;
        }

        //结果
        let result = "";
        //循环数据
        for (let i = 0; i < ids.length; i++) {
            let updateSql = "update ?? set status = ? ,deliveryAt=? where orderId = ?";
            let res = await mysql.query(updateSql, ["shop_order_list", 2, new Date().toLocaleString(), ids[i]]);
            if (!res) {
                result = "服务端数据库操作失败";
                break;
            }
        }
        if (result === "") {
            ctx.body = {code: 200, message: "success", data: ""};
        } else {
            ctx.body = {code: -3, message: result, data: ""};
        }

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//获取所有退款订单的数据
module.exports.getRefundOrderInfo = async (ctx) => {

    async function getArrayOfRefund(sql, params) {
        let res = await mysql.query(sql, params);
        if (!res) {
            return [];
        }
        return res;
    }

    try {
        // 获取参数
        let postParams = ctx.request.body;

        let {orderId, status, startTime, endTime, pageNumber, pageSize, refundId} = postParams;
        // 进行数据获取的判断
        let refundArray = "";
        pageSize = parseInt(pageSize);
        let start = (pageNumber - 1) * pageSize;

        if (orderId === "" && status === "" && refundId === "" && startTime === "" && endTime === "") {
            // 情况一没有任何数据
            let sql = "select * from ?? limit ?,?";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", start, pageSize]);
        } else if (orderId !== "" && status === "" && refundId === "" && startTime === "" && endTime === "") {
            //情况二（只能通过orderId来获取数据）
            let sql = "select * from ?? where orderId = ? ";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", orderId]);

        } else if (orderId === "" && status !== "" && refundId === "" && startTime === "" && endTime === "") {
            // 情况三（只能通过status来获取数据）
            let sql = "select * from ?? where status = ? limit ?,?";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", parseInt(status), start, pageSize]);

        } else if (orderId === "" && status === "" && refundId !== "" && startTime === "" && endTime === "") {
            // 情况四（只能通过refundId来获取数据）
            let sql = "select * from ?? where refundId = ? ";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", refundId]);
        } else if (orderId === "" && status === "" && refundId === "" && startTime !== "" && endTime !== "") {
            // 情况五（只能通过status和订单号 时间段来获取）
            let sql = "select * from ?? where createdAt between ? and ? ";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", startTime, endTime]);
        } else if (orderId !== "" && status !== "" && refundId === "" && startTime === "" && endTime === "") {
            // 情况六（只能通过status和订单号 refundId来获取来获取数据）
            let sql = "select * from ?? where status = ? and orderId= ? ";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", status, orderId]);
        } else if (orderId !== "" && status === "" && refundId !== "" && startTime !== "" && endTime !== "") {
            // 情况七（只能通过refundId和订单号 orderId来获取来获取数据）
            let sql = "select * from ?? where orderId = ? and refundId= ? ";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", orderId, refundId]);
        } else if (orderId !== "" && status === "" && refundId === "" && startTime !== "" && endTime !== "") {
            // 情况八 在某个时间范围内搜索某个orderId
            let sql = "select * from ?? where  orderId= ?  and createdAt between ? and ?  limit ?,?";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", orderId, startTime, endTime, start, pageSize]);

        } else if (orderId === "" && status !== "" && refundId === "" && startTime !== "" && endTime !== "") {
            //  情况六 在某个时间范围内搜索某个
            let sql = "select * from ?? where  status= ?  and createdAt between ? and ? limit ?,?";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", status, startTime, endTime, start, pageSize]);

        } else if (orderId === "" && status === "" && refundId !== "" && startTime !== "" && endTime !== "") {
            // 情况五 在某个时间范围内搜索某个
            let sql = "select * from ?? where  refundId= ? and status=? and createdAt between ? and ? ";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", refundId, status, startTime, endTime]);

        } else if (orderId !== "" && status !== "" && refundId !== "" && startTime !== "" && endTime !== "") {
            let sql = "select * from ?? where  orderId= ? and status=? and refundId=? and createdAt between ? and ? ";
            refundArray = await getArrayOfRefund(sql, ["shop_refund_record", orderId, status, refundId, startTime, endTime]);
        }

        //获取记录的总数
        let total = await mysql.query("select count(*) from ?? ", ["shop_refund_record"]);

        //获取所有数据
        if (!refundArray) {
            ctx.body = {code: -1, message: "获取数据失败", data: ""};
            return;
        }

        if (refundArray.length <= 0) {
            ctx.body = {code: 200, message: "", data: {refundArray: [], total: total[0]["count(*)"]}};
            return;
        }

        //处理服务区图片地址
        let photoImage = ProcessingPhotoAddresses.ProcessingPhotoAddresses(refundArray, "imageUrl");

        //获取用户数据
        for (let i = 0; i < refundArray.length; i++) {
            let userInfo = await mysql.query("select account from ?? where id = ?", ["shop_user_list", refundArray[i].userId]);
            photoImage[i].account = userInfo[0].account;
            delete photoImage[i].userId;
        }

        if (refundArray.length > 0) {
            ctx.body = {code: 200, message: "", data: {refundArray: photoImage, total: total[0]["count(*)"]}};
        }

    } catch (error) {
        ctx.body = {code: 500, message: "", data: error};
    }
};

//通过orderId获取用户的订单的详细信息
module.exports.getOrderDetailInfoById = async (ctx) => {
    try {
        //获取客户端的信息
        let {orderId} = ctx.request.body;
        // 获取数据
        let orderInfo = await mysql.query("select * from ?? where orderId = ?", ["shop_order_list", orderId]);
        // 订单不存在
        if (orderInfo.length <= 0) {
            ctx.body = {code: -1, message: "订单号不存在", data: ""};
            return;
        }

        // 获取数据信息
        let bookSql = "select name,price,salePrice,imageUrl from ?? where id = ?";
        let bookInfo = await mysql.query(bookSql, ["shop_book_list", orderInfo[0].bookId]);
        if (bookInfo.length <= 0) {
            ctx.body = {code: -2, message: "订单号不存在", data: ""};
            return;
        }

        //获取用户信息
        let userInfo = await mysql.query("select account from ?? where id = ?", ["shop_user_list", orderInfo[0].userId]);
        if (userInfo.length <= 0) {
            ctx.body = {code: -2, message: "订单号不存在", data: ""};
            return;
        }

        //获取快递公司信息
        let deliveryInfo = await mysql.query("select name from ?? where id = ?", ["shop_delivery_company", orderInfo[0]["deliveryId"]]);
        if (deliveryInfo.length <= 0) {
            ctx.body = {code: -2, message: "订单号不存在", data: ""};
            return;
        }

        //获取地址信息
        let addressInfo = await mysql.query("select province,city,county,addressDetail from ?? where id = ?", ["shop_user_delivery", orderInfo[0]["deliveryAddressId"]]);
        if (addressInfo.length <= 0) {
            ctx.body = {code: -2, message: "订单号不存在", data: ""};
            return;
        }

        //构建新对象
        let orderDetail = {
            orderId: orderInfo[0].orderId,
            account: userInfo[0].account,
            status: orderInfo[0].status,
            count: orderInfo[0].count,
            bookInfo: {
                name: bookInfo[0].name,
                price: bookInfo[0].price,
                salePrice: bookInfo[0].salePrice,
                imageUrl: Config.baseURL + ":" + Config.port + bookInfo[0].imageUrl.substring(16),
            },
            shopMoney: orderInfo[0].shopMoney,
            freight: orderInfo[0].freight,
            realPay: orderInfo[0].freight + orderInfo[0].shopMoney,
            deliveryName: deliveryInfo[0].name,
            courierNumber: orderInfo[0].courierNumber,
            receivingAddress: addressInfo[0].province + " " + addressInfo[0].city + " " + addressInfo[0].county + " " + addressInfo[0]["addressDetail"],
            createdAt: orderInfo[0].createdAt,
            deliveryAt: orderInfo[0].deliveryAt,
            finishAt: orderInfo[0].finishAt
        };

        ctx.body = {code: 200, message: "success", data: orderDetail};
    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//删除订单信息
module.exports.deleteOrder = async (ctx) => {
    try {
        let {ids} = ctx.request.body;

        if (ids.length <= 0) {
            ctx.body = {code: 200, message: "没有选择订单号", data: ""};
            return;
        }

        //删除的结果
        let result = "";
        for (let i = 0; i < ids.length; i++) {

            //先查询数据是否有该数据
            let orderInfo = await mysql.query("select * from ?? where orderId = ?", ["shop_order_list", ids[i]]);

            if (!orderInfo) {
                result = "服务端数据库错误";
                break;
            }

            //数据存在就删除
            if (orderInfo.length > 0) {
                //退款订单号
                let refundId = orderInfo[0]["refundId"]; //记录退款订单号
                // 判断获取的数据中是否有退款订单号 如果没有这直接删除 有的话获取退款订单号删除
                if (refundId === "" || refundId === null) {
                    let deleteOrderInfo = await mysql.query("delete from ?? where orderId=?", ["shop_order_list", ids[i]]);
                    if (!deleteOrderInfo) {
                        result = "删除失败"
                    }
                    result = "删除成功"
                } else {
                    // 不为空的情况 （删除整个订单）
                    let deleteOrderInfo = await mysql.query("delete from ?? where orderId=?", ["shop_order_list", ids[i]]);
                    if (!deleteOrderInfo) {
                        result = "删除失败"
                    }


                    // let RefundInfo = await mysql.query("select *  from ?? where refundId=?", ["shop_refund_record", refundId]);
                    // 删除退款图片地址
                    // let imageUrl = RefundInfo[0]["imageUrl"].toString().substring(16);


                    // 删除退款订单
                    let deleteRefundInfo = await mysql.query("delete from ?? where refundId=?", ["shop_refund_record", refundId]);
                    if (!deleteRefundInfo) {
                        result = "删除失败"
                    }

                    //判断文件是否存在 存在则删(需要改进)
                    // let res = await new Promise((resolve, reject) => {
                    //     fs.access(path.join(__dirname, "../public", imageUrl), (err) => {
                    //         //    文件和目录不存在的情况下；
                    //         if (err.code === "ENOENT") {
                    //             reject("文件和目录不存在")
                    //         }
                    //         resolve(true);
                    //     })
                    // });
                    //
                    // //文件存在删除
                    // if (res) {
                    //     let deleteFile = await DocumentFunction.deleteFile(path.join(__dirname, "../public", imageUrl));
                    //     if (!deleteFile) {
                    //         result = "删除失败"
                    //     }
                    //     result = "删除成功"
                    // }
                    result = "删除成功"
                }
            } else {
                result = "删除失败"
            }
        }

        if (result === "删除成功") {
            ctx.body = {code: 200, message: "success", data: ""};
        } else {
            ctx.body = {code: -4, message: result, data: ""};
        }

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务器端错误", data: error};
    }
};

//获取公司的信息
module.exports.getAllDeliveryCompany = async (ctx) => {
    try {
        let result = await mysql.query("select * from ?? ", ["shop_delivery_company"]);
        if (result.length <= 0) {
            ctx.body = {code: 200, message: "", data: []};
        } else {
            ctx.body = {code: 200, message: "", data: result};
        }
    } catch (error) {
        ctx.body = {code: 500, message: "", data: error};
    }
};

//删除快递公司的信息
module.exports.deleteDeliveryCompany = async (ctx) => {
    try {
        let {id} = ctx.request.body;
        let result = await mysql.query("select * from ?? where id = ?", ["shop_delivery_company", id]);
        if (result.length <= 0) {
            ctx.body = {code: -1, message: "删除失败", data: ""};
            return;
        }
        let deleteInfo = await mysql.query("delete from ?? where id= ?", ["shop_delivery_company", id]);

        if (!deleteInfo) {
            ctx.body = {code: -2, message: "删除失败", data: ""};
        } else {
            ctx.body = {code: 200, message: "删除成功", data: ""};
        }
    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//通过名字搜索名字信息
module.exports.filterByName = async (ctx) => {
    try {
        let {keywords} = ctx.request.body;
        //检查数据有效
        if (keywords === "" && keywords === undefined) {
            ctx.body = {code: -1, message: "参数错误", data: ""};
            return;
        }
        //查询数据
        let result = await mysql.query("select * from ?? where name =?", ["shop_delivery_company", keywords]);
        ctx.body = {code: 200, message: "success", data: result};
    } catch (e) {
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//添加物流公司的信息
module.exports.addDeliveryCompany = async (ctx) => {
    try {
        let {deliveryCompanyName} = ctx.request.body;
        if (deliveryCompanyName === null || deliveryCompanyName === undefined) {
            ctx.body = {code: -1, message: "提交数据不能为空"};
            return;
        }

        //实现数据的插入
        let insertData = [null, deliveryCompanyName];
        let insertInfo = await mysql.query("insert into  ?? values ?", ["shop_delivery_company", [insertData]]);
        if (!insertInfo) {
            ctx.body = {code: -2, message: "添加失败", data: ""};
        } else {
            ctx.body = {code: 200, message: "success", data: ""};
        }

    } catch (error) {

    }
};

//下载快递公司的数据上传模板
module.exports.downloadDeliveryCompany = async (ctx) => {
    try {
        //表头
        const _headers = ['id', "name"];
        let data = [];
        data.unshift(_headers);
        let buffer = NXlSX.build([{name: "sheetName", data: data}]);
        ctx.set("Content-Disposition", "attachment;filename=download.xls");
        // 返回buffer流到前端
        ctx.body = buffer;

    } catch (error) {
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//上传文件快递公司数据
module.exports.uploadDeliveryExcel = async (ctx) => {
    try {
        //获取客户端文件
        const files = ctx.request.files;

        // 检查文件是否为空
        if (files.excel.name === null) {
            let deleteInfo = await DocumentFunction.deleteFile(files.excel.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -2, message: "临时文件删除失败", data: ""};
            } else {
                ctx.body = {code: -1, message: "文件上传失败", data: "2"};
            }
            return;
        }

        //检查文件类型
        if (![".xls", ".xlsx"].includes(path.extname(files.excel.name))) {
            let deleteInfo = await DocumentFunction.deleteFile(files.excel.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -2, message: "临时文件删除失败", data: ""};
            } else {
                ctx.body = {code: -1, message: "文件上传失败", data: "1"};
            }
            return;
        }

        // 使用模块解析Excel表格
        const workSheetsFromBuffer = NXlSX.parse(fs.readFileSync(files.excel.path));

        let count = 0;
        // 检查表格的格式是否正确
        for (let i = 0; i < workSheetsFromBuffer.length; i++) {
            if (workSheetsFromBuffer[i].data[0][0] !== "id" || workSheetsFromBuffer[i].data[0][1] !== "name") {
                continue;
            }
            count++;
        }
        if (count !== workSheetsFromBuffer.length) {
            let deleteInfo = await DocumentFunction.deleteFile(files.excel.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -2, message: "临时文件删除失败", data: ""};
            } else {
                ctx.body = {code: -3, message: "上传的数据表格有问题", data: ""};
            }
            return;
        }

        // 取出表格中的数据
        let tableData = [];
        for (let i = 0; i < workSheetsFromBuffer.length; i++) {
            // 删除每个工作表的表头
            let len = workSheetsFromBuffer[i].data.length;
            let workSheetsFromFile = workSheetsFromBuffer[i].data.slice(1, len);

            for (let j = 0; j < workSheetsFromFile.length; j++) {
                tableData.push(workSheetsFromFile[j]);
            }
        }

        // 先检查每一项的数据在数据库中是否存在
        let checkArray = [];
        for (let i = 0; i < tableData.length; i++) {
            //检查数据是否重复
            let result = await mysql.query("select * from ?? where id = ?", ["shop_delivery_company", tableData[i][0]]);

            //数据为空的时候
            if (!result || result.length === 0) {
                checkArray = [];
            } else {
                checkArray.push(i);
            }
        }

        //检查数据
        if (checkArray.length > 0) {
            let deleteInfo = await DocumentFunction.deleteFile(files.excel.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -2, message: "临时文件删除失败", data: ""};
            } else {
                ctx.body = {code: -4, message: "表格中除表头外，第" + checkArray + "条有问题", data: ""};
            }
            return;
        }

        //插入数据
        let insertInfo = await mysql.query("insert into ?? values ?", ["shop_delivery_company", tableData]);

        if (!insertInfo) {
            ctx.body = {code: -5, message: "插入失败", data: ""};
        } else {
            let deleteInfo = await DocumentFunction.deleteFile(files.excel.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -2, message: "临时文件删除失败", data: ""};
            }
            ctx.body = {code: 200, message: "添加成功", data: ""};
        }

    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//实现订单的发货功能
module.exports.doDeliveryComplete = async (ctx) => {
    try {
        //获取客户端参数
        let {orderId} = ctx.request.body;

        //检查参数实现订单的发货功能能
        if (orderId === "" || orderId === undefined || JSON.stringify(orderId) === "[]") {
            ctx.body = {code: -1, message: "数据不完整", data: ""};
            return;
        }
        //获取数据的长度是否为空
        if (orderId.length <= 0) {
            ctx.body = {code: -2, message: "请选择要发货的订单", data: ""};
            return;
        }

        //订单结果
        let resultOrder = "";
        let resultArray = [];
        //循环数据判断是否该订单号在数据库中是否存在
        for (let i = 0; i < orderId.length; i++) {
            //查询数据判断是否有该数据
            let res = await mysql.query("select * from ?? where orderId=?", ["shop_order_list", orderId[i]]);
            if (!res) {
                resultOrder = "服务端操作数据库失败";
                break;
            }
            if (res.length <= 0) {
                resultOrder = "记录" + orderId[i] + "不存在";
                break;
            }
            resultArray.push(res[0]);
        }

        //判断结果
        if (resultOrder !== "") {
            ctx.body = {code: -3, message: resultOrder, data: ""};
            return;
        }


        //需要修改的数据
        let updateOrder = "";
        let insertInfo = "";
        //循环数据对对该有的订单进行相应的数据进行修改
        for (let i = 0; i < resultArray.length; i++) {
            // 判断数组中每个对象的数据
            if ((resultArray[i]["courierNumber"] !== "" || resultArray[i]["courierNumber"] !== null) && resultArray[i]["status"] === 2) {
                let updateSql = "update ?? set status = ? where orderId=?";
                let updateRes = await mysql.query(updateSql, ["shop_order_list", 3, resultArray[i]["orderId"]]);
                if (!updateRes) {
                    updateOrder = "服务端操作数据库失败";
                    break;
                }

                //同时添加一条物流消息
                let insertData = [resultArray[i]["orderId"], "包裹已出库", new Date().toLocaleString()];
                let insertInfo = await mysql.query("insert into ?? values ?", ["shop_logisticsInfo_list", [insertData]]);
                if (!insertInfo) {
                    insertInfo = "服务端操作数据库失败";
                    break;
                }
            } else {
                updateOrder = "请检查当前状态或者是否选择了快递公司";
            }
        }

        //判断数据
        if (updateOrder !== "" || insertInfo !== "") {
            ctx.body = {code: -4, message: updateOrder, data: ""};
        } else {
            ctx.body = {code: 200, message: "success", data: ""};
        }

    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端程序错误", data: e};
    }
};

//实现退款订单的删除功能
module.exports.deleteRefundOrder = async (ctx) => {
    try {
        // 获取参数
        let {ids} = ctx.request.body;
        // 判断检查参数
        if (ids === "" || ids === undefined) {
            ctx.body = {code: -1, message: "获取数据为空", data: ""};
            return;
        }
        if (ids.length <= 0) {
            ctx.body = {code: -2, message: "获取数据为空", data: ""};
            return;
        }

        let selectInfo = "";
        //存储数据
        let result = [];
        //循环获取数据
        for (let i = 0; i < ids.length; i++) {
            let res = await mysql.query("select orderId,imageUrl from ?? where orderId=?", ["shop_refund_record", ids[i]]);
            if (!res) {
                selectInfo = "服务端操作数据库失败";
                break;
            }
            result.push(res[0]);
        }
        if (selectInfo !== "") {
            ctx.body = {code: -3, message: selectInfo, data: ""};
            return;
        }
        if (result.length <= 0) {
            ctx.body = {code: -4, message: "找不到要删除的记录", data: ""};
            return;
        }

        //结果
        let deleteInfo = "";
        //删除数据
        for (let i = 0; i < result.length; i++) {
            //①删除每个数据的退款记录
            let res1 = await mysql.query("delete  from ?? where orderId = ?", ["shop_refund_record", result[i]["orderId"]]);
            if (!res1) {
                deleteInfo = "服务端数据库操作失败";
                break;
            }
            //②删除每个退款订单的记录
            let res2 = await mysql.query("delete  from ?? where orderId = ?", ["shop_order_list", result[i]["orderId"]]);
            if (!res2) {
                deleteInfo = "服务端数据库操作失败";
                break;
            }
            //③删除除退款订单对应的图片数据
            // let src = result[i]["imageUrl"].toString().substring(16, result[i]["imageUrl"].length);
            //
            // //删除文件
            // let deleteFile = await DocumentFunction.deleteFile(path.join(__dirname, "../public", src));
            //
            // if (deleteFile === -1) {
            //     deleteInfo = "服务端删除文件失败";
            //     break;
            // }
        }


        if (deleteInfo === "") {
            ctx.body = {code: 200, message: "success", data: ""};
        } else {
            ctx.body = {code: -5, message: deleteInfo, data: ""};
        }


    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端程序错误", data: e};
    }
};