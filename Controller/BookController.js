const mysql = require("./OperateDB/mysql.js");
const OperationalObjectives = require("./Function/OperationalObjectives.js");
const DocumentFunction = require("./Function/DocumentFunction.js");
const ProcessingPhotoAddresses = require("./Function/ProcessingPhotoAddresses.js");
const Config = require("../config/config.js");
const path = require("path");
const NXlSX = require("node-xlsx");
const TraversingArray = require("./Function/TraversingArray.js");

//获取所有的图书数据
module.exports.getAllBookList = async (ctx) => {

    async function getArrayOfOrder(sql, params) {
        let res = await mysql.query(sql, params);
        if (!res) {
            return [];
        }
        return res;
    }

    try {
        let postParams = ctx.request.body;
        let {name, press, author, startTime, endTime, pageNumber, pageSize} = postParams;
        // 进行数据获取的判断
        let bookArray = "";
        pageSize = parseInt(pageSize);
        let start = (pageNumber - 1) * pageSize;
        if (name === "" && press === "" && author === "" && startTime === "" && endTime === "") {
            // 情况一 没有任何参数
            let sql = "select * from ??  limit ?,?";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", start, pageSize]);
        } else if (name !== "" && press === "" && author === "" && startTime === "" && endTime === "") {
            //情况二 通过 orderId来获取
            let sql = "select * from ?? where name = ?";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", name]);

        } else if (name === "" && press !== "" && author === "" && startTime === "" && endTime === "") {
            // 情况三（只能通过status来获取数据）
            let sql = "select * from ?? where press = ? limit ?,?";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", press, start, pageSize]);

        } else if (name === "" && press === "" && author !== "" && startTime === "" && endTime === "") {
            // 情况三（只能通过status来获取数据）
            let sql = "select * from ?? where author = ? limit ?,?";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", author, start, pageSize]);

        } else if (name === "" && press === "" && author === "" && startTime !== "" && endTime !== "") {
            // 情况四（只能通过 时间段 来获取数据）
            let sql = "select * from ?? where createdAt between ? and ? ";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", startTime, endTime]);

        } else if (name !== "" && press !== "" && author === "" && startTime === "" && endTime === "") {
            // 情况五（只能通过status和订单号 orderId来获取来获取数据）
            let sql = "select * from ?? where name = ? and press= ? ";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", name, press]);

        } else if (name !== "" && press === "" && author === "" && startTime !== "" && endTime !== "") {
            // 情况六 在某个时间范围内搜索某个orderId
            let sql = "select * from ?? where  name= ?  and createdAt between ? and ? ";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", name, startTime, endTime]);

        } else if (name === "" && press !== "" && startTime !== "" && endTime !== "") {
            // 情况七 在某个时间范围内搜索某个
            let sql = "select * from ?? where  press= ?  and createdAt between ? and ? limit ?,?";
            bookArray = await getArrayOfOrder(sql, ["shop_book_list", press, startTime, endTime, start, pageSize]);
        } else if (name !== "" && press !== "" && author !== "" && startTime !== "" && endTime !== "") {
            // 情况八 在某个时间范围内搜索某个
            let sql = "select * from ?? where  name= ? and press=? and author=? and createdAt between ? and ? ";
            bookArray = await getArrayOfOrder(sql, ["shop_order_list", name, press, author, startTime, endTime]);
        }

        //获取所有数据
        if (bookArray.length <= 0) {
            ctx.body = {code: 200, message: "图书列表为空", data: []};
            return;
        }

        //删除对象数组中对象的属性
        let bookResults = bookArray;

        // 循环数据数据
        for (let i = 0; i < bookResults.length; i++) {
            bookResults[i]["classify"] = bookResults[i]["classify"].split(",");
            //修改图片的地址
            bookResults[i]["imageUrl"] = ProcessingPhotoAddresses.ShowsDifferentPaths(bookResults[i]["imageUrl"]);
        }

        let total = await mysql.query("select count(*) from ??", ["shop_book_list"]);
        ctx.body = {code: 200, message: "success", data: {bookResults, total: total[0]["count(*)"]}};

    } catch (error) {
        console.log(error);
        ctx.body = {code: 500, message: "服务端错误", data: error};
    }
};

//获取所有的书本分类数据
module.exports.getAllBookCategoryList = async (ctx) => {
    try {
        let result = await mysql.query("select * from ?? ", ["shop_book_category"]);

        if (!result) {
            ctx.body = {code: -1, message: "获取数据失败", data: ""};
            return;
        }

        if (result.length <= 0) {
            ctx.body = {code: -2, message: "获取数据失败", data: ""};
            return;
        }
        ctx.body = {code: 200, message: "success", data: result};

    } catch (e) {
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//修改书本数据
module.exports.doEditBookInfo = async (ctx) => {
    let postFile = ctx.request.files;
    try {
        let postParams = ctx.request.body;
        let {id, name, author, press, classify, title, description, price, salePrice, isSell} = postParams;

        //检查数据的有效性
        let bookInfo = await mysql.query("select * from ?? where id = ?", ["shop_book_list", id]);

        let bookImageUrl = bookInfo[0].imageUrl;

        //数据出现错误
        if (!bookInfo) {
            ctx.body = {code: -1, message: "服务端数据库错误", data: ""};
            return;
        }
        //数据记录不存在
        if (bookInfo.length <= 0) {
            ctx.body = {code: -1, message: "该记录不存在，无法修改", data: ""};
            return;
        }

        // 删除数据数组对象属性
        let deleteBookInfo = OperationalObjectives.DeleteAttribute(bookInfo, ["stock", "stockPrice", "imageUrl", "createdAt", "updatedAt"]);


        // 判断是否一样进行返回（数据一样不需要修改）
        if (JSON.stringify(postFile) === "{}") {

            // 删除对象属性
            delete postParams.imageUrl;

            // 判断数据是否一样
            //这个数组存储的是客户端传递过来的数据
            let postParamsValue = [];
            for (let item in postParams) {
                postParams.id = parseInt(postParams["id"]);
                postParams.price = parseFloat(postParams["price"]);
                postParams.salePrice = parseFloat(postParams["salePrice"]);
                postParams.isSell = parseInt(postParams["isSell"]);
                if (postParams.hasOwnProperty(item)) {
                    postParamsValue.push(postParams[item]);
                }
            }

            //这个数组存储的是数据库的数据
            let bookInfoValue = [];
            for (let key in deleteBookInfo[0]) {
                if (deleteBookInfo[0].hasOwnProperty(key)) {
                    bookInfoValue.push(bookInfo[0][key]);
                }
            }

            //记录判断结果
            let flag = true;

            //循环判断是否一样
            for (let i = 0; i < postParamsValue.length; i++) {
                if (postParamsValue[i] !== bookInfoValue[i]) {
                    flag = false;
                }
            }
            //发回结果
            if (flag) {
                ctx.body = {code: -1, message: "和之前记录一样，无需修改", data: ""};
                return;
            }

            //修改数据库；
            let sql = "update ?? set name=?,author=?,press=?,classify=?,title=?,description=?,price=?,salePrice=?,isSell=?,updatedAt=? where id = ?";
            let params = ["shop_book_list", name, author, press, classify, title, description, price, salePrice, isSell, new Date().toLocaleString(), id];
            let result = await mysql.query(sql, params);
            if (result) {
                ctx.body = {code: 200, message: "success", data: ""};
                return;
            } else {
                ctx.body = {code: -2, message: "服务端操作数据库失败", data: ""};
                return;
            }
        }

        // 文件为空
        if (postFile.imageUrl.name === "") {
            let deleteInfo = await DocumentFunction.deleteFile(postFile.imageUrl.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -3, message: "临时文件删除失败", data: ""};
            } else {
                ctx.body = {code: -4, message: "请上传图片文件", data: ""};
            }
            return;
        }

        //文件类型
        if (![".jpg", ".png", ".JPG", ".PNG",".jpeg",".JPEG"].includes(path.extname(postFile.imageUrl.name))) {
            let deleteInfo = await DocumentFunction.deleteFile(postFile.imageUrl.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -3, message: "临时文件删除失败", data: ""};
            }
            ctx.body = {code: -4, message: "图片文件类型不正确", data: ""};
            return;
        }

        //创建一个文件名
        const filename = DocumentFunction.createFileName(new Date()) + path.extname(postFile.imageUrl.name);

        //重名文件
        let newPath = path.join(__dirname, "../public/images/book/", filename);
        const renameFile = await DocumentFunction.renameFile(postFile.imageUrl.path, newPath);

        // 重命名失败
        if (renameFile === -1) {
            //重命名失败
            let deleteInfo = await DocumentFunction.deleteFile(postFile.imageUrl.path);
            if (deleteInfo === -1) {
                ctx.body = {code: -3, message: "临时文件删除失败", data: ""};
            }
            ctx.body = {code: -5, message: "重命名失败", data: ""};
            return;
        }

        //修改数据数据
        let sql = "update ?? set name=?,author=?,press=?,classify=?,title=?,description=?,price=?,salePrice=?,isSell=?,imageUrl=? ,updatedAt=?where id = ?";
        let imageUrl = Config.baseURL + "/images/book/" + filename;
        let updateParams = ["shop_book_list", name, author, press, classify, title, description, price, salePrice, isSell, imageUrl, new Date().toLocaleString(), id];
        let updateInfo = await mysql.query(sql, updateParams);

        if (!updateInfo) {
            ctx.body = {code: -5, message: "服务端修改数据库记录错误", data: ""};
            return;
        }

        //删除旧文件
        let oldSrc = bookImageUrl.substring(16);
        let deleteInfo = await DocumentFunction.deleteFile(path.join(__dirname, "../", "public", oldSrc));
        if (deleteInfo === -1) {
            ctx.body = {code: -3, message: "临时文件删除失败", data: ""};
        }

        ctx.body = {code: 200, message: "success", data: ""};

    } catch (e) {
        console.log(e);
        let deleteInfo = await DocumentFunction.deleteFile(postFile.excel.path);
        if (deleteInfo === -1) {
            ctx.body = {code: -3, message: "临时文件删除失败", data: ""};
        } else {
            ctx.body = {code: -4, message: "", data: ""};
        }
        ctx.body = {code: 500, message: " 服务器错误", data: e};
    }
};

//实现实现所有的书本的上架和下架
module.exports.changeBookSellStatus = async (ctx) => {
    try {
        let postParams = ctx.request.body;
        let {isSell, ids} = postParams;
        if (isSell === "" || isSell === undefined) {
            ctx.body = {code: -1, message: "请上传上架或者下架状态", data: ""};
            return;
        }
        if (ids.length === 0) {
            ctx.body = {code: -2, message: "请选择书本", data: ""};
            return false;
        }

        // 检查数据是否存在
        let count = 0;
        for (let i = 0; i < ids.length; i++) {
            let result = await mysql.query("select * from ?? where id = ?", ["shop_book_list", ids[i]]);
            if (!result) {
                break;
            }
            if (result.length <= 0) {
                continue;
            }
            count++;
        }
        //判断是否有该对象
        if (count !== ids.length) {
            ctx.body = {code: -3, message: "编号为" + count + "记录不存在", data: ""};
            return;
        }


        let result = "";
        //修改数据数据数据
        for (let i = 0; i < ids.length; i++) {
            let updateInfo = await mysql.query("update  ?? set isSell=? where id = ?", ["shop_book_list", isSell, ids[i]]);
            if (!updateInfo) {
                result = "服务端操作数据库失败";
            }
        }
        if (result === "") {
            ctx.body = {code: 200, message: "修改成功", data: ""};
        } else {
            ctx.body = {code: -5, message: "修改失败", data: ""};
        }


    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//实现书籍的删除
module.exports.deleteBook = async (ctx) => {
    try {
        //客户端id
        let {ids} = ctx.request.body;

        if (ids.length <= 0) {
            ctx.body = {code: -1, message: "删除失败，请选择要删除的项", data: ""};
            return;
        }

        //循环数组删除数据
        let result = "";
        for (let i = 0; i < ids.length; i++) {
            //查询数据是否有存在在的话删除数据
            let resultInfo = await mysql.query("select * from ?? where id = ?", ["shop_book_list", ids[i]]);
            if (resultInfo.length <= 0) {
                continue;
            }
            let filePath = resultInfo[0].imageUrl.substring(16);
            let deleteInfo = await mysql.query("delete from ?? where id=?", ["shop_book_list", ids[i]]);
            if (deleteInfo === -1) {
                result = "数据库记录删除失败";
                return;
            }
            //删除所对应的文件
            let deleteFileInfo = await DocumentFunction.deleteFile(path.join(__dirname, "../public", filePath));
            if (deleteFileInfo === -1) {
                result = "该记录对应的图片文件删除失败";
            }
        }
        if (result === "") {
            ctx.body = {code: 200, message: "success", data: ""};
        } else {
            ctx.body = {code: -3, message: result, data: ""};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务器端错误", data: ""};

    }
};

//实现书籍模板文件的下载
module.exports.downloadTemplate = async (ctx) => {
    try {
        //表头
        const _headers = ['编号', "书名", "作者", "出版社", "分类（1-n）表示", "标题", "描述", "库存", "库存价", "价格", "售价", "是否在售（0,1表示）", "图片地址"];
        let data = [];
        data.unshift(_headers);
        let buffer = NXlSX.build([{name: "sheetName", data: data}]);
        ctx.set("Content-Disposition", "attachment;filename=download.xls");
        // 返回buffer流到前端
        ctx.body = buffer;
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务器端错误", data: ""};
    }
};

//实现书籍的批量导入
module.exports.leadingInExcel = async (ctx) => {
    let files = ctx.request.files;
    try {
        //检查数据文件的类型
        if (files.excel.name === "") {
            let deleteInfo = await DocumentFunction.deleteFile(files.excel.path);
            if (deleteInfo === -1) {
                ctx.body = {code: "-1", message: "服务器删除临时文件失败", data: ""};
            } else {
                ctx.body = {code: "-2", message: "请上传数据文件", data: ""};
            }
            return;
        }
        //文件类型
        if (![".xls", ".xlsx"].includes(path.extname(files.excel.name))) {
            let deleteInfo = await DocumentFunction.deleteFile(files.excel.path);
            if (deleteInfo === -1) {
                ctx.body = {code: "-1", message: "服务器删除临时文件失败", data: ""};
            } else {
                ctx.body = {code: "-2", message: "请上传Excel文件", data: ""};
            }
            return;
        }
        //解析数据
        const workSheetsFromBuffer = NXlSX.parse(files.excel.path);

        // 循环数据
        let tableData = [];
        for (let i = 0; i < workSheetsFromBuffer.length; i++) {
            let workSheetsData = workSheetsFromBuffer[i].data;
            let data = workSheetsData.slice(1, workSheetsData.length);
            for (let j = 0; j < data.length; j++) {
                tableData.push(data[j]);
            }
        }

        // 添加数据字段
        for (let i = 0; i < tableData.length; i++) {
            let createAt = new Date().toLocaleString();
            let updateAt = new Date().toLocaleString();
            tableData[i].push(createAt);
            tableData[i].push(updateAt);
        }

        // 检查数据是否有效重复
        let checkData = [];
        for (let i = 0; i < tableData.length; i++) {
            let result = await mysql.query("select * from ?? where id =?", ["shop_book_list", tableData[i][0]]);
            if (result.length > 0) {
                checkData.push(i);
            }
        }

        if (checkData.length <= 0) {
            let result = await mysql.query("insert into ?? values ?", ["shop_book_list", tableData]);
            if (result) {
                ctx.body = {code: 200, message: "success", data: ""}
            }
        } else {
            ctx.body = {code: -3, message: "插入失败", data: ""}
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e}
    }
};

//实现书籍信息的导出
module.exports.leadingOutExcel = async (ctx) => {
    try {
        //读取数据数据
        let resultArray = await mysql.query("select * from ?? ", ["shop_book_list"]);
        let deleteBookInfo = OperationalObjectives.DeleteAttribute(resultArray, ["status"]);
        // 构建表头
        const _headers = ['编号', "书名", "作者", "出版社", "分类（1-n）表示", "标题", "描述", "库存", "库存价", "价格", "售价", "是否在售（0,1表示）", "图片地址", "创建时间", "修改时间"];
        let ConvertingResult = TraversingArray.ObjectArrayConvertingToTwoDimension(deleteBookInfo);
        // 加入表头
        ConvertingResult.unshift(_headers);

        // 返回buffer对象
        ctx.body = NXlSX.build([{name: "mySheetName", data: ConvertingResult}]);


    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: ""};
    }
};

//获取书籍的分类
module.exports.getBookClassify = async (ctx) => {
    try {
        let result = await mysql.query("select * from ?? ", ["shop_book_category"]);
        if (!result) {
            ctx.body = {code: -1, message: "获取数据失败", data: ""};
            return;
        }
        ctx.body = {code: 200, message: "success", data: result};
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: ""};

    }
};

//删除书籍分类逻辑业务
module.exports.deleteBookCategory = async (ctx) => {
    try {
        let {id} = ctx.request.body;
        let ids = id;

        // 查询是否有该id
        let resultInfo = await mysql.query("Select * from ?? where id  = ?", ["shop_book_category", ids]);
        if (!resultInfo) {
            ctx.body = {code: -1, message: "服务端操作数据库失败", data: ""};
            return;
        }
        if (resultInfo.length <= 0) {
            ctx.body = {code: -2, message: "删除失败，该分类不存在", data: ""};
            return;
        }
        let deleteInfo = await mysql.query("delete from ?? where id =?", ["shop_book_category", ids]);
        if (!deleteInfo) {
            ctx.body = {code: -3, message: "服务端操作数据库失败", data: ""};
        } else {
            ctx.body = {code: 200, message: "删除成功", data: ""};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//添加书籍分类
module.exports.addClassifyName = async (ctx) => {
    try {
        let {classifyName} = ctx.request.body;
        if (classifyName === "" || classifyName === undefined) {
            ctx.body = {code: -1, message: "添加分类失败", data: ""};
            return;
        }
        let insertData = [null, classifyName];
        let result = await mysql.query("insert into ?? values ? ", ["shop_book_category", [insertData]]);
        if (!result) {
            ctx.body = {code: -2, message: "添加分类失败", data: ""};
        } else {
            ctx.body = {code: 200, message: "success", data: ""};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};