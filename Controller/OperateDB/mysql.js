const MySql = require("mysql"),
    Config = require("../../config/config.js");

//  单例模式 ES6
class Mysql {
    //用户实现单例共享 减少了实例对象耗费的时间
    static getInstance() {
        //单例已经实现了
        if (!Mysql.instance) {
            Mysql.instance = new Mysql();
        }
        return Mysql.instance;
    }

    constructor() {
        this.connection = "";//表示的是一个进行数据的连接状态对象的属性
        this.connect(); //表示的是一个进行数据的连接的放法
    }

    //这是一个连接的方法
    connect() {
        //只要属性值不为空可以不改变其值创建一个连接对象然后进行连接 如果this.connection对象存在直接连接不用创建对象
        try {
            if (!this.connection) {
                this.connection = MySql.createConnection(Config.mysql);
                this.connection.connect();
            } else {
                //表示已经创建了一connection对象 可以直接使用
                this.connection.connect();
            }
        } catch (err) {
            console.log(err.message);
        }
    }

    //该方法用户数据的操作
    query(sql, params = []) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, params, function (err, results, fields) {
                if (err) {
                    console.log(err);
                    reject(false);
                    return;
                }
                // 返回成功的数据
                resolve(results);
            });
        });
    }
}

// 暴露函数
module.exports = Mysql.getInstance();