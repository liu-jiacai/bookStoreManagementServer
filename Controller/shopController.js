//引入数据库模块
const mysql = require("./OperateDB/mysql.js");

//获取商店店铺的信息
module.exports.getShopInfo = async (ctx) => {
    try {
        //获取客户端参数
        //sql 语句
        let sql = "select * from ??";
        let result = await mysql.query(sql, ["shop_info"]);

        if (!result) {
            ctx.body = {code: -1, message: "没有查找到数据的结果", data: ""};
        } else {
            ctx.body = {code: 200, message: "success", data: result[0]};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//修改店铺的信息（店铺名称）
module.exports.updateShopName = async (ctx) => {
    try {
        let params = ctx.request.body;
        if (params.id === "") {
            ctx.body = {code: -1, message: "改书店的ID不存在，不能修改", data: ""};
            return;
        }
        if (params["shopName"] === "") {
            ctx.body = {code: -2, message: "请填写书店的名称", data: ""};
            return;
        }

        //修改数据
        let result = await mysql.query("update  ?? set shopName=?,updatedAt=? where id = ?", ["shop_info", params["shopName"], new Date().toLocaleString(), params.id]);
        if (!result) {
            ctx.body = {code: -3, message: "修改失败", data: ""};
        } else {
            ctx.body = {code: 200, message: "success", data: ""};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//修改店铺的信息（店铺开关）
module.exports.updateShopStatus = async (ctx) => {
    try {
        let params = ctx.request.body;
        if (params.id === "") {
            ctx.body = {code: -1, message: "改书店的ID不存在，不能修改", data: ""};
            return;
        }
        if (params.status === "") {
            ctx.body = {code: -2, message: "请选择店铺的状态", data: ""};
            return;
        }

        //修改数据
        let result = await mysql.query("update  ?? set status=?,updatedAt=? where id = ?", ["shop_info", params.status, new Date().toLocaleString(), params.id]);
        if (!result) {
            ctx.body = {code: -3, message: "修改失败", data: ""};
        } else {
            ctx.body = {code: 200, message: "success", data: ""};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};

//修改店铺的信息（店铺描述）
module.exports.updateShopDesc = async (ctx) => {
    try {
        let params = ctx.request.body;
        if (params.id === "") {
            ctx.body = {code: -1, message: "改书店的ID不存在，不能修改", data: ""};
            return;
        }
        if (params.description === "") {
            ctx.body = {code: -2, message: "请填写店铺的详细信息", data: ""};
            return;
        }

        //修改数据
        let result = await mysql.query("update  ?? set description=?,updatedAt=? where id = ?", ["shop_info", params.description, new Date().toLocaleString(), params.id]);
        if (!result) {
            ctx.body = {code: -3, message: "修改失败", data: ""};
        } else {
            ctx.body = {code: 200, message: "success", data: ""};
        }
    } catch (e) {
        console.log(e);
        ctx.body = {code: 500, message: "服务端错误", data: e};
    }
};