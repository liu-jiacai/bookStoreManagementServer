const Config = require("../../config/config.js");

//修改数组中的图片地址
function ProcessingPhotoAddresses(array, attr) {
    if (array.length <= 0) {
        return [];
    }
    for (let i = 0; i < array.length; i++) {
        array[i][attr] = Config.dataURL + array[i][attr].substring(16, array[i][attr].length);
    }
    return array;
}

//修改数组中的某个字段的数值（保留两位小数）
function ProcessingArrayValue(array, attr) {
    if (array.length <= 0) {
        return [];
    }
    for (let i = 0; i < array.length; i++) {
        array[i][attr] = array[i][attr].toFixed(2);
    }
    return array;
}

//判断图片的地址使用不同路径进行显示
function ShowsDifferentPaths(str) {
    // 如果要检索的字符串值没有出现，则该方法返回 -1。
    if (str.indexOf("ddimg.cn") === -1) {
        return Config.baseURL + ":" + Config.port + str.substring(16, str.length);
    } else {
        return str;
    }
}

//暴露方法
module.exports = {ProcessingPhotoAddresses, ProcessingArrayValue, ShowsDifferentPaths};