//订单号生成模块
function orderCode() {
    const now = new Date();
    let month = (now.getMonth() + 1).toString().padStart(2, "0");
    let day = now.getDate().toString().padStart(2, "0");
    let hour = now.getHours().toString().padStart(2, "0");
    let seconds = now.getSeconds().toString().padStart(3, "0");
    return now.getFullYear() + month + day + hour + seconds + (Math.round(Math.random() * 10000)).toString();
}

//退款流水号生成方法
function  RefundOrderNumber() {
    let courierNumber = '';
    for (let i = 0; i < 3; i++) //3位随机数，用以加在时间戳后面。
    {
        courierNumber += Math.floor(Math.random() * 10);
    }
    return "T" + new Date().getTime() + courierNumber;  //时间戳，用来生成订单号。
}

//暴露模块
module.exports = {RefundOrderNumber};
