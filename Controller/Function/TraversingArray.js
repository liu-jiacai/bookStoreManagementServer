//JS数组中对象属性(bookId)相同对某值进行相加
function mergeArray(array, mergeKeywords, field1, field2) {
    let newArray = [];
    if (array.length <= 0) {
        return newArray;
    }
    let temp = {};   //临时对象
    for (let i = 0; i < array.length; i++) {
        let key = array[i][mergeKeywords]; //判断依据
        if (temp[key]) {
            temp[key][mergeKeywords] = key;
            temp[key][field1] = array[i][field1];
            temp[key][field2] += array[i][field2];//相加值
        } else {
            temp[key] = {};
            temp[key][mergeKeywords] = key;
            temp[key][field1] = array[i][field1];
            temp[key][field2] = array[i][field2];
        }
    }

    for (let k in temp) {
        newArray.push(temp[k]);
    }
    return newArray;
}

//JS数组中对象属性(bookId)相同对某值进行相加 方法2
function mergeArray2(array, mergeKeywords, field1, field2) {
    let newArray = [];
    if (array.length <= 0) {
        return newArray;
    }
    let temp = {};   //临时对象
    for (let i = 0; i < array.length; i++) {
        let key = array[i][mergeKeywords]; //判断依据
        if (temp[key]) {
            temp[key][mergeKeywords] = key;
            temp[key][field2] += array[i][field1] + array[i][field2];//相加值
        } else {
            temp[key] = {};
            temp[key][mergeKeywords] = key;
            temp[key][field2] = array[i][field1] + array[i][field2];
        }
    }

    for (let k in temp) {
        newArray.push(temp[k]);
    }
    return newArray;
}

//对象数据排序 （只针对于数值型数据，时间也可以）（倒叙排列）
function sortArray(Array, sortValue) {
    if (Array.length <= 0) {
        return [];
    }
    let b = Array;
    for (let i = 0; i < b.length - 1; i++) {
        //每循环遍历一次，数组b中都有一个元素确定位置，所以减少i次
        for (let j = 0; j < b.length - 1 - i; j++) {
            //这里按照将数据从小到大的顺序进行排序，前面的元素比后面的小，交换位置
            if (b[j][sortValue] < b[j + 1][sortValue]) {
                let temp = b[j + 1];
                b[j + 1] = b[j];
                b[j] = temp;
            }
        }
    }
    return b;
}

//数据没有达到指定长度填充空对象
function fillingData(array, obj) {
    let newArray = array;
    let nowLength = array.length;
    for (let i = 0; i < 10 - nowLength; i++) {
        newArray.push(obj);
    }
    return newArray;
}

//过滤数组中的数组
function filterArray(array, field) {
    let data1 = [];
    let data2 = [];
    for (let i = 0; i < array.length; i++) {
        if (array[i].hasOwnProperty(field) && array[i][field] !== null) {
            data1.push(array[i]);
        } else {
            data2.push(array[i]);
        }
    }
    return {accordWith: data1, noAccordWith: data2};
}

//把一个对象数组 转换换成二维数组
function ObjectArrayConvertingToTwoDimension(array) {
    let newArr = [];
    for (let i = 0; i < array.length; i++) {
        let objectArr = Object.values(array[i]);
        newArr.push(objectArr);
    }
    return newArr;
}

module.exports = {mergeArray, sortArray, fillingData, mergeArray2, ObjectArrayConvertingToTwoDimension};