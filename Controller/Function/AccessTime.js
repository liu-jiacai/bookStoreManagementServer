//获取当天时间时间段
const theWholeDay = function theWholeDay(date) {
    let timeStart = new Date(new Date(date.getTime()).setHours(0, 0, 0, 0));
    let endTime = new Date(new Date(date.getTime()).setHours(23, 59, 59, 999));
    return {timeStart, endTime};
};

//获取当前时间的这一周
const currentWeek = function currentWeek(date) {
    let nowTime = new Date(date);

    // 表示的星期几
    let dayOfTheWeek = nowTime.getDay(); //表示当前时间需要往前数几天
    let distanceNextWeek = 6 - dayOfTheWeek; //表示当前时间需要往后数几天

    // 获取当前时间的天数
    let nowOfDate = nowTime.getDate(); //每个月的几号

    //设置二月天数
    let the_year = nowTime.getFullYear();
    let February = 0;
    if ((the_year % 4 === 0 && the_year % 100 !== 0) || (the_year % 400 === 0) || (the_year % 3200 === 0 && the_year % 172800 === 0)) {
        February = 29;
    } else {
        February = 28;
    }

    // 12个月每个月的天数
    let MonthDays = [31, February, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    let nowMonth = nowTime.getMonth();
    let updateEndTime = "";
    if (nowOfDate + distanceNextWeek > MonthDays[nowMonth]) {
        updateEndTime = new Date(new Date(nowTime.setMonth(nowMonth + 2)).setDate(nowOfDate + distanceNextWeek))
    } else {
        updateEndTime = new Date(nowTime.setDate(nowOfDate + distanceNextWeek));
    }

    // 修改日期
    let updateTimeStart = new Date(nowTime.setDate(nowOfDate - dayOfTheWeek));

    // 修改时分秒毫秒
    let timeStart = new Date(updateTimeStart.setHours(0, 0, 0, 0));
    let endTime = new Date(updateEndTime.setHours(23, 59, 59, 999));
    return {timeStart, endTime};
};

//当前日期在的这一个月
const currentMonth = function currentMonth(date) {
    const nowTime = new Date(date);
    const currentMonth = nowTime.getMonth(); //（获取当前月需要加1）
    let startCurrentDate = new Date(nowTime.setDate(1));  //设置当前月的1号

    let the_year = nowTime.getFullYear();
    let February = 0;
    // 判断闰年
    if ((the_year % 4 === 0 && the_year % 100 !== 0) || (the_year % 400 === 0) || (the_year % 3200 === 0 && the_year % 172800 === 0)) {
        February = 29;
    } else {
        February = 28;
    }
    let MonthDate = [31, February, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    let endCurrentDate = new Date(nowTime.setDate(MonthDate[currentMonth]));
    let timeStart = new Date(startCurrentDate.setHours(0, 0, 0, 0));
    let endTime = new Date(endCurrentDate.setHours(23, 59, 59, 999));
    return {timeStart, endTime};
};


//当一个时间段
const MonthQuantum = function MonthQuantum(date) {
    const nowTime = new Date(date);
    const currentMonth = nowTime.getMonth() + 1; //（获取当前月需要加1）

    //设置二月天数
    let the_year = nowTime.getFullYear();
    let February = 0;
    // 判断闰年
    if ((the_year % 4 === 0 && the_year % 100 !== 0) || (the_year % 400 === 0) || (the_year % 3200 === 0 && the_year % 172800 === 0)) {
        February = 29;
    } else {
        February = 28;
    }
    let MonthDate = [31, February, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    let monthRange = currentMonth - 7;
    //7个月前
    let beforeMonthDate = new Date(nowTime.setDate(1));
    let beforeMonth = new Date(beforeMonthDate.setMonth(monthRange));
    let startTime = new Date(beforeMonth.setHours(0, 0, 0, 0));

    // //设置本月日期
    let endCurrentDate = new Date(nowTime.setDate(MonthDate[currentMonth - 1]));
    let endTime = new Date(endCurrentDate.setHours(23, 59, 59, 999));
    return {startTime, endTime};
};

//暴露
module.exports = {currentWeek, currentMonth, theWholeDay, MonthQuantum};