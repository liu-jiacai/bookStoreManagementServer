const fs = require("fs");

//生成文件名
let createFileName = function createFileName(date) {
    let dt = new Date(date);
    let year = dt.getFullYear();
    let month = dt.getMonth().toString().padStart(2, "0");
    let day = dt.getDay().toString().padStart(2, "0");
    let hours = dt.getHours().toString().padStart(2, "0");
    let minutes = dt.getMinutes().toString().padStart(2, "0");
    let seconds = dt.getSeconds().toString().padStart(2, "0");
    let milliSeconds = dt.getMilliseconds().toString().padStart(3, "0");
    let random = parseInt(Math.random() * 10);
    return year + month + day + hours + minutes + seconds + milliSeconds + random;
};

// 重命名文件
let renameFile = function renameFile(oldPath, newPath) {
    return new Promise((resolve, reject) => {
        let isExist = fs.existsSync(oldPath);
        if (isExist) {
            fs.rename(oldPath, newPath, (err) => {
                if (err) {
                    console.log(err);
                    reject(-1);
                }
                resolve('重命名完成');
            });
        } else {
            console.log("文件不存在，重名失败");
            reject(-1);
        }
    });
};

//删除文件
let deleteFile = function deleteFile(path) {
    return new Promise((resolve, reject) => {
        let isExist = fs.existsSync(path);
        if (isExist) {
            fs.unlink(path, (err) => {
                if (err) {
                    console.log(err);
                    reject(-1);
                }
                resolve('文件已被删除');
            });
        } else {
            console.log("文件不存在");
            resolve('文件已被删除');
        }
    });
};

//读取文件
let readFile = function readFile(path) {
    return new Promise((resolve, reject) => {
        let isExist = fs.existsSync(path);
        if (isExist) {
            fs.readFile(path, "utf8", (err, data) => {
                if (err) {
                    console.log(err);
                    reject(-1);
                }
                resolve(data);
            });
        } else {
            console.log("文件不存在");
            reject(-1);
        }
    });
};

module.exports = {createFileName, renameFile, deleteFile, readFile};