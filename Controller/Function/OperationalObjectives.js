//删除对象数组中的对象的多个属性
const DeleteAttribute = function DeleteAttribute(resultArray, deleteAttributeArray) {
    let array = resultArray;
    if (array.length <= 0) {
        return [];
    }
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < deleteAttributeArray.length; j++) {
            if (array[i].hasOwnProperty(deleteAttributeArray[j])) {
                delete array[i][deleteAttributeArray[j]];
            } else {
                continue;
            }
        }
    }
    return array;
};

//满足某些条件删除某个特定的属性
const DeleteSpeciallyAttribute = function DeleteSpeciallyAttribute(array, deleteAttribute) {
    if (array.length <= 0) {
        return [];
    }
    for (let i = 0, len = array.length; i < len; i++) {
        if (array[i].hasOwnProperty(deleteAttribute) && !(array[i][deleteAttribute] === null || array[i][deleteAttribute] === undefined)) {
            delete array[i][deleteAttribute];//删除
        } else {
            continue;
        }
    }
    return array;
};

//给对象数组的每个对象元素添加属性
const AddSpeciallyAttribute = function AddSpeciallyAttribute(array, addAttribute, addAttributeValue) {
    if (array.length <= 0) {
        return [];
    }
    for (let i = 0, len = array.length; i < len; i++) {
        array[i][addAttribute] = addAttributeValue;
    }
    return array;
};

//给对象数组的每个对象元素添加多个属性
const AddAttribute = function AddAttribute(array, addAttributeArray, addAttributeValueArray) {
    if (array.length <= 0) {
        return [];
    }

    if (addAttributeArray.length !== addAttributeValueArray.length) {
        return [];
    }
    for (let i = 0, len = array.length; i < len; i++) {
        for (let j = 0; j < addAttributeArray.length; j++) {
            array[i][addAttributeArray[j]] = addAttributeValueArray[j];
        }
    }
    return array;
};


//修改对象数组的某些特定属性名称
const changeSpeciallyAttribute = function AddSpeciallyAttribute(array, changeAttribute, changedName) {
    if (array.length <= 0) {
        return [];
    }
    for (let i = 0, len = array.length; i < len; i++) {
        //先添加属性
        array[i][changedName] = array[i][changeAttribute];
        delete array[i][changeAttribute];
    }
    return array;
};

//暴露
module.exports = {
    DeleteAttribute,
    DeleteSpeciallyAttribute,
    AddSpeciallyAttribute,
    changeSpeciallyAttribute,
    AddAttribute
};