//引入对用户密码的加密加盐的模块
const bCrypt = require("bcrypt");

const saltRounds = 10;

/**
 * 函数说明：这个函数用来对密码来进行加盐加密处理
 *参数说明
 * 参数1：需要加密的密码
 * */
module.exports.SaltEncryption = (password) => {
    // 使用Promise
    return new Promise((resolve, reject) => {
        bCrypt.genSalt(saltRounds, function (err, salt) {
            if (err) {
                reject(err);
            }
            bCrypt.hash(password, salt, function (err, hash) {
                if (err) {
                    reject(err);
                }
                resolve(hash);
            });
        });
    });
};

module.exports.PasswordCompare = (password, hash) => {
    // 使用Promise
    return new Promise((resolve, reject) => {
        bCrypt.compare(password, hash, function (err, result) {
            if (err) {
                reject(err);
            }
            resolve(result);
        });
    });
};

