const keys = require("./keys.js");
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const mysql = require("../Controller/OperateDB/mysql.js");
var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secretOrPrivateKey;


module.exports = passport => {
    passport.use(new JwtStrategy(opts, async (jwt_payload, done) => {
        const user = await mysql.query("select * from ?? where account = ?", ["shop_admin_user", jwt_payload.account]);
        if (user) {
            return done(null, user[0]);
        } else {
            return done(null, false);
        }
    }));
};
