module.exports = {
    mysql: {
        host: '127.0.0.1',   // 数据库地址
        user: 'root',    // 数据库用户
        password: 'abc123',   // 数据库密码（本地）
        // password: '123456',   // 数据库密码（服务器）
        database: "shop_store_management"  // 选中数据库
    },
    port: 7070,

    // 改地址主要是为了获取用户的头像地址
    // 本地
    dataURL: "http://127.0.0.1:3001",
    // 服务器
    // dataURL: "http://47.104.16.62:3001",

    // 后台数据地址
    // 本地
    baseURL: "http://127.0.0.1",
    // 服务器
    // baseURL: "http://47.104.16.62",
};