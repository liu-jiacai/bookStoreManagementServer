//引入模块并实例化
const Koa = require("koa");
const app = new Koa();

// 引用Koa-Router并实例化路由对象
const router = new require("koa-router")();

//配置passport初始化
const passport = require("koa-passport");
app.use(passport.initialize());
require("./config/passport.js")(passport);


//处理post请求模块
const path = require("path");
const koaBody = require("koa-body");
app.use(koaBody({
    multipart: true, // 支持文件上传
    formidable: {
        uploadDir: path.join(__dirname, 'public/images/user'), // 设置文件上传目录
        keepExtensions: true,    // 保持文件的后缀
        maxFieldsSize: 500 * 1024, // 文件上传大小
    }
}));

// 实现静态资源托管
const serve = require("koa-static");
app.use(serve(path.join(__dirname, "public")));

// 引入路由模块并挂载
const api = require("./Router/api.js");
router.use("/api", api);

//引用跨域库模块并设置跨域
const cors = require("@koa/cors");
app.use(cors());

//启动路由
app.use(router.routes());
app.use(router.allowedMethods());

//监听
app.listen(7070, () => {
    console.log("Server is running on 7070 port");
});