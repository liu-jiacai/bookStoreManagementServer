//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入token验证
const passport = require("koa-passport");

//引入逻辑处理模块
const OrderListController = require("./../../Controller/OrderListController.js");

//获取所有的订单数据  passport.authenticate("jwt", {session: false}),
router.post("/allOrderList", passport.authenticate("jwt", {session: false}), OrderListController.getAllOrderList);

// 实现快递公司的修改和快递单号的修改
router.post("/editDeliveryInfo", passport.authenticate("jwt", {session: false}), OrderListController.editDeliveryInfo);

//获取当前订单的物流信息
router.post("/addLogisticsInfo", passport.authenticate("jwt", {session: false}), OrderListController.addLogisticsInfo);

//获取当前订单的物流信息
router.post("/getLogisticsInfo", passport.authenticate("jwt", {session: false}), OrderListController.getLogisticsInfo);

//获取当前订单的收货地址
router.post("/getDeliveryAddress", passport.authenticate("jwt", {session: false}), OrderListController.getDeliveryAddress);

//获取用户的收货地址列表
router.post("/getUserAddressList", passport.authenticate("jwt", {session: false}), OrderListController.getUserAddressList);

//修改用户的收货地址
router.post("/updateDeliveryAddress", passport.authenticate("jwt", {session: false}), OrderListController.updateDeliveryAddress);

//获取省市县信息
router.get("/getAreaDataInfo", passport.authenticate("jwt", {session: false}), OrderListController.getAreaDataInfo);

//添加新的收货地址
router.post("/addAddressInfo", passport.authenticate("jwt", {session: false}), OrderListController.addAddressInfo);

//确定订单的路由
router.post("/confirmOrder", passport.authenticate("jwt", {session: false}), OrderListController.confirmOrder);

//处理退款的路由
router.post("/submitRefundOrder", passport.authenticate("jwt", {session: false}), OrderListController.submitRefundOrder);

//获取退款订单的数据
router.post("/getRefundOrderInfo", passport.authenticate("jwt", {session: false}), OrderListController.getRefundOrderInfo);

//获取订单的详细数据
router.post("/getRefundOrderDetailInfo", passport.authenticate("jwt", {session: false}), OrderListController.getOrderDetailInfoById);

//删除订单的详细数据
router.post("/deleteOrder", passport.authenticate("jwt", {session: false}), OrderListController.deleteOrder);

//获取公司的信息
router.get("/getAllDeliveryCompany", passport.authenticate("jwt", {session: false}), OrderListController.getAllDeliveryCompany);

//删除快递公司的信息
router.post("/deleteDeliveryCompany", passport.authenticate("jwt", {session: false}), OrderListController.deleteDeliveryCompany);

//删除快递公司的信息
router.post("/filterByName", passport.authenticate("jwt", {session: false}), OrderListController.filterByName);

//添加物流信息
router.post("/addDeliveryCompany", passport.authenticate("jwt", {session: false}), OrderListController.addDeliveryCompany);

//添加物流下载物流公司模板
router.post("/downloadDeliveryCompany", passport.authenticate("jwt", {session: false}), OrderListController.downloadDeliveryCompany);

//上传文件
router.post("/uploadDeliveryExcel", passport.authenticate("jwt", {session: false}), OrderListController.uploadDeliveryExcel);


//实现订单的发货前功能
router.post("/doDeliveryComplete", passport.authenticate("jwt", {session: false}), OrderListController.doDeliveryComplete);

//删除退款记录功能
router.post("/deleteRefundOrder", passport.authenticate("jwt", {session: false}), OrderListController.deleteRefundOrder);


//暴露
module.exports = router.routes();


