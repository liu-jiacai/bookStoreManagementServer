//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入token验证
const passport = require("koa-passport");

//引入用户模块的逻辑处理模块
const UserController = require("../../Controller/UserController.js");

/************************具体匹配用户模块的路由****************************/
//用户注册的路由
router.post("/register", UserController.doRegister);

//用户登录的路由
router.post("/login", UserController.doLogin);

//修改用户昵称的路由
router.post("/changeNickName", passport.authenticate("jwt", {session: false}), UserController.changeNickName);

//修改用户的头像
router.post("/changeAvatar", passport.authenticate("jwt", {session: false}), UserController.changeAvatar);

//修改用户的登录密码路由
router.post("/changePassword", passport.authenticate("jwt", {session: false}), UserController.changePassword);

//获取当前用户的信息
router.get("/current", passport.authenticate("jwt", {session: false}), UserController.getCurrentInfo);


/*******************************管理普通用户的所有路由********************************/
//获取用户的列表
router.post("/allUserInfo",passport.authenticate("jwt", {session: false}), UserController.getAllUserInfo);

// 重置用户密码
router.post("/resetUserPwd", passport.authenticate("jwt", {session: false}),UserController.resetPassword);

//改变用户的状态
router.post("/changeUserStatus", passport.authenticate("jwt", {session: false}),UserController.changeUserStatus);


//获取用户的收货地址
router.post("/getUserDeliveryAddress",passport.authenticate("jwt", {session: false}), UserController.getUserDeliveryAddress);

//删除数据用户
router.post("/deleteUser", passport.authenticate("jwt", {session: false}),UserController.deleteUser);

//添加用户数据
router.post("/addUser", passport.authenticate("jwt", {session: false}),UserController.addUser);

//实现用户数据的导出用户数据
router.post("/exportExcel",passport.authenticate("jwt", {session: false}), UserController.downloadExcel);

//暴露user模块
module.exports = router.routes();