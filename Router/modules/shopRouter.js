//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入token验证
const passport = require("koa-passport");

//引入逻辑处理模块
const shopController = require("../../Controller/shopController.js");


//获取商店店铺的信息
router.get("/getShopInfo", passport.authenticate("jwt", {session: false}),shopController.getShopInfo);

//修改店铺的信息（店铺名称）
router.post("/updateShopName",passport.authenticate("jwt", {session: false}), shopController.updateShopName);

//修改店铺的信息（店铺开关）
router.post("/updateShopStatus",passport.authenticate("jwt", {session: false}), shopController.updateShopStatus);

//修改店铺的信息（店铺描述）
router.post("/updateShopDesc", passport.authenticate("jwt", {session: false}),shopController.updateShopDesc);


//暴露user模块
module.exports = router.routes();