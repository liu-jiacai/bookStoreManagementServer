//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入token验证
const passport = require("koa-passport");

//引入用户模块的逻辑处理模块
const FeedbackController = require("../../Controller/FeedbackController.js");


//获取所有的反馈信息
router.post("/getFeedBackRecordList", passport.authenticate("jwt", {session: false}),FeedbackController.getFeedBackRecordList);

//处理反馈信息
router.post("/handleFeedBack",passport.authenticate("jwt", {session: false}), FeedbackController.handleFeedBack);


//删除反馈记录
router.post("/deleteFeedBackRecord",passport.authenticate("jwt", {session: false}), FeedbackController.deleteFeedBackRecord);

//暴露
module.exports = router.routes();