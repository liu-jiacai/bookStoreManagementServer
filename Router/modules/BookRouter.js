//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入token验证
const passport = require("koa-passport");

//引入逻辑处理模块
const BookController = require("./../../Controller/BookController.js");


//获取所有的书本数据 passport.authenticate("jwt", {session: false}),
router.post("/allBookList",passport.authenticate("jwt", {session: false}), BookController.getAllBookList);

//获取所有的书本的分类的信息  passport.authenticate("jwt", {session: false}),
router.get("/allBookCategoryList",passport.authenticate("jwt", {session: false}), BookController.getAllBookCategoryList);

//编辑书本数据
router.post("/editBook",passport.authenticate("jwt", {session: false}), BookController.doEditBookInfo);

//改变书本的的上架和下架
router.post("/changeBookSellStatus",passport.authenticate("jwt", {session: false}), BookController.changeBookSellStatus);

//删除书籍
router.post("/deleteBook",passport.authenticate("jwt", {session: false}), BookController.deleteBook);

//实现书籍模板文件的下载路由
router.post("/downloadTemplate",passport.authenticate("jwt", {session: false}), BookController.downloadTemplate);

//实现书籍数据的批量导入数据
router.post("/leadingInExcel",passport.authenticate("jwt", {session: false}), BookController.leadingInExcel);

//实现书籍数据的批量导出数据
router.post("/leadingOutExcel",passport.authenticate("jwt", {session: false}), BookController.leadingOutExcel);

//获取书籍的分类
router.get("/getBookClassify",passport.authenticate("jwt", {session: false}), BookController.getBookClassify);

//删除书籍的分类
router.post("/deleteBookCategory",passport.authenticate("jwt", {session: false}), BookController.deleteBookCategory);

//添加书本分类
router.post("/addClassifyName", passport.authenticate("jwt", {session: false}),BookController.addClassifyName);

module.exports = router.routes();