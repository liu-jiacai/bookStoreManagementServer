//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入token验证
const passport = require("koa-passport");

//引入用户模块的逻辑处理模块
const indexController = require("../../Controller/IndexController.js");

//获取今日数据要处理的数据（今日订单,未处理订单，退款订单）
//修改用户昵称的路由
router.get("/getOrderStatistics", passport.authenticate("jwt", {session: false}), indexController.getOrderStatistics);


//获取日周月销量/销售额的路由
router.post("/getOrderStatisticsByType", passport.authenticate("jwt", {session: false}), indexController.getOrderStatisticsByType);

//获取日周月销量/销售额的趋势变化
router.post("/getTrendInfoByType", passport.authenticate("jwt", {session: false}), indexController.getTrendInfoByType);

// 获取前世销量
router.get("/getTop10Info",passport.authenticate("jwt", {session: false}), indexController.getTop10Info);

//暴露模块
module.exports = router.routes();