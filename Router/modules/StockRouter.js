//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入token验证
const passport = require("koa-passport");

//引入用户模块的逻辑处理模块
const StockController = require("../../Controller/StockController.js");


//获取所有的进货数据信息
router.post("/getStockRecordList", passport.authenticate("jwt", {session: false}),StockController.getStockRecordList);

//获取书籍的部分信息
router.get("/getBookPartInfo",passport.authenticate("jwt", {session: false}), StockController.getBookPartInfo);

//增加所有的进货数据信息
router.post("/addStockRecord",passport.authenticate("jwt", {session: false}), StockController.addIncomingData);

//增加所有的进货数据信息
router.post("/deleteStockRecord",passport.authenticate("jwt", {session: false}), StockController.deleteStockRecord);


//暴露user模块
module.exports = router.routes();