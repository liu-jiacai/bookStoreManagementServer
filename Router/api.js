//引入koa-router对象并实例化
const router = new require("koa-router")();

//引入路由模块
const UserRouter = require("./modules/UserRouter.js");
const IndexRouter = require("./modules/IndexRouter.js");
const OrderRouter = require("./modules/OrderRouter.js");
const BookRouter = require("./modules/BookRouter.js");
const StockRouter = require("./modules/StockRouter.js");
const FeedbackRouter = require("./modules/FeedbackRouter.js");
const Shop = require("./modules/shopRouter.js");


// 挂载到对象上
router.use("/user", UserRouter);
router.use("/index", IndexRouter);
router.use("/order", OrderRouter);
router.use("/book", BookRouter);
router.use("/stock", StockRouter);
router.use("/feedback", FeedbackRouter);
router.use("/shop", Shop);


// 暴露路由对象
module.exports = router.routes();